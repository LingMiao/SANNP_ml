#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    References
    ----------
Created on Thu Sep 20 12:15:58 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

import os
import numpy as np
import cupy as cp

import parameters as pm
from func_struct import get_atoms_type

class EiNN_cupy:
    """
    Ei Neural Network (EiNN), cupy version

    parameters:

    """

    def __init__(self, \
            nodes_In=pm.nFeats, nodes_L1=pm.nNodeL1, nodes_L2=pm.nNodeL2, \
            maxNb=pm.maxNb, f_atoms=pm.f_atoms, f_Wij_np=pm.f_Wij_np):

        self.nIn = nodes_In
        self.nL1 = nodes_L1
        self.nL2 = nodes_L2
        self.nInMax = nodes_In.max()
        self.maxNb  = maxNb

        self.itp_uc, self.at_types, _ = get_atoms_type(f_atoms)
        assert cp.all(self.at_types==pm.at_types), "please check at_types in parameter.py, it should be as above value!"
        self.natoms = cp.sum(self.at_types[:,1]) 
        self.ntypes = self.at_types.shape[0]

        self.nnWij = self.loadWij_np(f_Wij_np, b_print=True)

    def _preprocessor(self, features):
        return 

    def sigmoid(self, x):
        return 1.0 /(1.0 +cp.exp(-x))

    def getEi_itp(self, itp, features):
        W1 = cp.asarray(self.nnWij[itp*6+0])
        B1 = cp.asarray(self.nnWij[itp*6+1])
        W2 = cp.asarray(self.nnWij[itp*6+2])
        B2 = cp.asarray(self.nnWij[itp*6+3])
        W3 = cp.asarray(self.nnWij[itp*6+4])
        B3 = cp.asarray(self.nnWij[itp*6+5])

        #L1out = self.sigmoid(cp.matmul(features, W1)+B1)
        L1out = self.sigmoid(cp.matmul(features[:,:self.nIn[itp]], W1)+B1)
        L2out = self.sigmoid(cp.matmul(L1out, W2)+B2)
        L3out = cp.matmul(L2out, W3)+B3
    
        return L3out

    def getEi(self, features, itypes):
        """
        """
        itypes = cp.reshape(itypes, [-1,1])

        Ei = cp.zeros_like(itypes, dtype=cp.float)
        for i in range(self.ntypes):
            idx = (itypes[:,0] == self.at_types[i, 0])
            Ei[idx] = self.getEi_itp(i, features[idx])

        return Ei
    
    def cp_get_dEldXi(self, itp, features):
        W1 = cp.asarray(self.nnWij[itp*6+0])
        B1 = cp.asarray(self.nnWij[itp*6+1])
        W2 = cp.asarray(self.nnWij[itp*6+2])
        B2 = cp.asarray(self.nnWij[itp*6+3])
        W3 = cp.asarray(self.nnWij[itp*6+4])
        B3 = cp.asarray(self.nnWij[itp*6+5])

        #L1out = self.sigmoid(cp.matmul(features, W1)+B1)
        L1out = self.sigmoid(cp.matmul(features[:,:self.nIn[itp]], W1)+B1)
        L2out = self.sigmoid(cp.matmul(L1out, W2)+B2)
            
        w_j = cp.sum(cp.expand_dims(L2out*(1-L2out),1) * \
                cp.expand_dims(W2,0) * \
                cp.expand_dims(cp.transpose(W3),0), axis=2)
        dEldXi = cp.sum(cp.expand_dims(L1out*(1-L1out)*w_j,1) * cp.expand_dims(W1,0),2)

        #o_zeros = cp.zeros((self.at_types[itp,1], self.nInMax -self.nIn[itp]))
        o_zeros = cp.zeros((features.shape[0], self.nInMax -self.nIn[itp]))
        cp_dEldXi = cp.concatenate([dEldXi,o_zeros], axis=1)
    
        return cp_dEldXi

    def getFi(self, features, tfdXi, tfdXin, cp_idxNb):
        """
        defult nImg=1
        """
        cp_dEldXi = cp.zeros_like(features, dtype=cp.float)
        for i in range(self.ntypes):
            idx = (self.itp_uc == self.at_types[i, 0])
            cp_dEldXi[idx] = self.cp_get_dEldXi(i, features[idx])

        #cp_dEldXi = self.cp_get_dEldXi(itype, features)
        Fll = cp.sum(cp.expand_dims(cp_dEldXi,2)*tfdXi,axis=1)
            
        dEnldXin = cp.zeros((self.natoms, self.maxNb, self.nInMax))
        dEnldXin[cp_idxNb>0] = cp_dEldXi[cp_idxNb[cp_idxNb>0].astype(int)-1]
        #print("getFi().dEnldXin \n", dEnldXin.shape )

        Fln = cp.sum(cp.expand_dims(dEnldXin,3)*tfdXin,axis=(1,2))

        return Fll + Fln

    def loadWij_np(self, f_Wij_np, b_print=False):
        nnWij = np.load(f_Wij_np)
        print('EiNN_cupy.loadWij_np from', f_Wij_np, nnWij.dtype, nnWij.shape)

        if b_print:
            for itp in range(self.ntypes):
                print('itype', itp)
                print('  W1', nnWij[itp*6+0].dtype, nnWij[itp*6+0].shape)
                print('  B1', nnWij[itp*6+1].dtype, nnWij[itp*6+1].shape)
                print('  W2', nnWij[itp*6+2].dtype, nnWij[itp*6+2].shape)
                print('  B2', nnWij[itp*6+3].dtype, nnWij[itp*6+3].shape)
                print('  W3', nnWij[itp*6+4].dtype, nnWij[itp*6+4].shape)
                print('  B3', nnWij[itp*6+5].dtype, nnWij[itp*6+5].shape)
        return nnWij


#===============================================================================
class NNapiBase:
    """
    NNapiBase, cupy version

    parameters:

    """

    #===========================================================================
    def __init__(self, nn, data_scaler):

        self.maxNb  = nn.maxNb
        self.natoms = nn.natoms
        self.itp_uc = nn.itp_uc
        self.ds = data_scaler
        self.nn = nn

