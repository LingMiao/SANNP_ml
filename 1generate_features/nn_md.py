#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 20 12:15:58 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

import parameters as pm
if pm.cupyMD:
    import cupy as cp
    from nn_model_cupy import NNapiBase #,EiNN_cupy
else:  # use tf version
    import numpy as cp
    import tensorflow as tf
    from nn_model import NNapiBase #,EiNN

import numpy as np
import pandas as pd
import sys
import time

from func_data_scaler import mae, mse, mse4, get_feat_scl_nbxyz, get_feat_dfeat_scl_nbxyz
from func_struct import Atoms_cupy, get_neighbor_struct_cp

from ase import Atoms
from ase.io import read, write
from ase.units import fs, Bohr, eV, Ang, kB, Hartree
#print('fs, Bohr, eV, Ang, kB\n', fs, Bohr, eV, Ang, kB)
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution

class NNMD(NNapiBase):
    """
    NNMD

    parameters:

    """

    #===========================================================================
    def __init__(self, nn, data_scaler):

        super(NNMD, self).__init__( nn=nn, data_scaler=data_scaler)
        
        # below vars setted in self.init_structure()
        self.atoms_ase = None
        # and transfer to GPU cupy arrays
        self.atoms_gpu = Atoms_cupy()

    #===========================================================================
    def getEp_uc(self, itype, cell, pos): 
        nNb,idxNb,nbxyz = get_neighbor_struct_cp(cell, pos)
        feat_scaled = get_feat_scl_nbxyz(itype, nNb, idxNb, nbxyz, self.ds)
        
        if pm.cupyMD:
            engy_out = self.nn.getEi(feat_scaled, itype)
        else:  # use tf version
            engy_out = \
                    self.sess.run( \
                    (self.pred_Ei), \
                    feed_dict={self.X:feat_scaled, self.itp:itype})

        Ep = self.ds.post_Ei(engy_out)

        return Ep, cp.sum(Ep)

    #===========================================================================
    #def getEpFp_nbxyz(self, itype, nNb, idxNb, nbxyz):
    def getEpFp_uc(self, itype, cell, pos): 
        nNb,idxNb,nbxyz = get_neighbor_struct_cp(cell, pos)
        feat_scaled, feat_d1, feat_d2 = get_feat_dfeat_scl_nbxyz(itype, nNb, idxNb, nbxyz, self.ds)
        
        if pm.cupyMD:
            engy_out = self.nn.getEi(feat_scaled, itype)
            f_out    = self.nn.getFi(feat_scaled, feat_d1, feat_d2, idxNb)
        else:  # use tf version
            engy_out, f_out = \
                    self.sess.run( \
                    (self.pred_Ei, self.pred_Fi), \
                    feed_dict={self.X:feat_scaled, \
                    self.tfdXi:feat_d1, self.tfdXin:feat_d2, self.tf_idxNb:idxNb, self.itp:self.itp_uc})

        Ep = self.ds.post_Ei(engy_out)
        Fp = self.ds.post_Fi(f_out)

        return Ep, Fp, cp.sum(Ep)

    #===========================================================================
    def check_EpFp(self, f_atoms, f_initial):
        # Wait debug
        df_Fi = self.init_structure(f_atoms, f_initial, b_restart=False, f_restart=None)
        Ei, f, Etot = self.getEpFp_uc(self.atoms_gpu.numbers, self.atoms_gpu.cell, self.atoms_gpu.positions)
        print("first structure mae(df_Fi, f))", mae(df_Fi, f))

    #===========================================================================
    def NVE_vv(self, f_atoms, f_initial, \
            b_restart=pm.b_restart, f_restart=pm.f_restart, savefile=pm.savefile, \
            dt=pm.dt, steps=pm.steps, temp0=pm.temp0,\
            n_print=pm.n_print, f_traj=pm.f_traj):
        """
        # verlet algrithm
        # NN force NAGETIVE from DFT calculation!!! 
        # So, +f should be -f here!!!
        """
        print("=Job start NNMD.NVE at ", time.ctime())
        df_Fi = self.init_structure(f_atoms, f_initial, b_restart=b_restart, f_restart=f_restart)
        Ei, f, Etot = self.getEpFp_uc(self.atoms_gpu.numbers, self.atoms_gpu.cell, self.atoms_gpu.positions)
        print("first structure mae(df_Fi, f))", mae(df_Fi, f))

        v = self.atoms_gpu.velocities
        r = self.atoms_gpu.get_positions(wrap=False)
        for istep in range(steps):
            v += 0.5 * dt * (-f) / self.atoms_gpu.masses *eV/Ang
            r += dt * v

            Ei, f, Etot = self.getEpFp_uc(self.atoms_gpu.numbers, self.atoms_gpu.cell, r)
        
            v += 0.5 * dt * (-f) / self.atoms_gpu.masses *eV/Ang
            
            self.atoms_gpu.positions  = r
            self.atoms_gpu.velocities = v
            self.md_log(dt, istep, Ei, f, Etot, n_print, f_traj)
        
        print("=Job end NNMD.NVE at ", time.ctime())

    #===========================================================================
    def init_structure(self, f_atoms, f_initial, b_restart=False, f_restart=None):
        # load ini structure
        # first get numpy arrays in CPU, 
        # then transfer to GPU cupy arrays
        if not self.atoms_ase:
            self.atoms_ase = read(f_atoms)

        self.natoms = len(self.atoms_ase)
        print(self.natoms, self.atoms_ase.get_cell())

        if b_restart == False:
            df_PosForsV = pd.read_csv(f_initial, \
                    header=None,index_col=False,dtype="float", delim_whitespace=True)
            df_Pos = df_PosForsV.iloc[0:self.natoms, 1:4].values
            self.atoms_ase.set_scaled_positions(df_Pos)
            df_Fi = df_PosForsV.iloc[self.natoms*1:self.natoms*2, 1:4].values
            df_Vi = df_PosForsV.iloc[self.natoms*2:self.natoms*3, 1:4].values
            self.atoms_ase.set_velocities(df_Vi*Bohr/fs)
            #NOTE: just for compare
            MaxwellBoltzmannDistribution(self.atoms_ase, pm.temp0*kB)  # 300K

            df_Ei = df_PosForsV.iloc[self.natoms*3:self.natoms*4, 1].values
            print("Will do NN_MD start as structure ", f_initial, \
                    "Epot", "{:8.4f}".format(cp.sum(df_Ei)), 
                    "Ep+Ek", "{:8.4f}".format( cp.sum(df_Ei) +self.atoms_ase.get_kinetic_energy() ))
        else:
            df_PosForsV = pd.read_csv(f_restart, \
                    header=None,index_col=False,dtype="float", delim_whitespace=True)
            df_Ei  = df_PosForsV.iloc[0:self.natoms, 1].values
            df_Pos = df_PosForsV.iloc[0:self.natoms, 2:5].values
            self.atoms_ase.set_positions(df_Pos)
            df_Fi = -df_PosForsV.iloc[0:self.natoms, 5:8].values
            df_Vi = df_PosForsV.iloc[0:self.natoms, 8:11].values
            self.atoms_ase.set_velocities(df_Vi)
            #MaxwellBoltzmannDistribution(self.atoms_ase, temp0*kB)  # 300K
            print("Will restart NN_MD as the last structure in ", f_restart, \
                    "Epot", "{:8.4f}".format(cp.sum(df_Ei)), 
                    "Ep+Ek", "{:8.4f}".format( cp.sum(df_Ei) +self.atoms_ase.get_kinetic_energy() ))
        
        pos_out = self.atoms_ase.get_scaled_positions(wrap=True)
        Vi_out = self.atoms_ase.get_velocities()
        print("pos (scaled) ", pos_out[0,:], "Vi (Bohr/fs)", Vi_out[0,:]/Bohr*fs)
        
        print("\n units of following data are t:fs, E:eV, T:K, xyz:Ang, Fxyz:eV/Ang, Vi:Ang/?s (?s = 1e-5 *sqrt(u['_e']/u['_amu'])")
        sys.stdout.flush()

        # cpu2gpu
        self.atoms_gpu.set_from_aseAtoms(self.atoms_ase)  # NOTE atoms_ase.copy() useful ?
        
        if pm.cupyMD:
            df_Fi = cp.asarray(df_Fi)
        return df_Fi

    #===========================================================================
    def init_structure_fix(self, f_atoms, f_fix, temp0):
        # load ini structure
        # first get numpy arrays in CPU, 
        # then transfer to GPU cupy arrays
        self.atoms_ase = read(f_atoms)

        self.natoms = len(self.atoms_ase)
        print(self.natoms, self.atoms_ase.get_cell())

        MaxwellBoltzmannDistribution(self.atoms_ase, temp0*kB)  
        print("Will do NN_MD start as structure ", f_atoms, \
                "temp", "{:8.4f}".format( self.atoms_ase.get_temperature() ))

        df_fix= pd.read_csv(f_fix, \
                header=None,index_col=False,dtype="int", delim_whitespace=True)
        fixmask = df_fix.iloc[:, 1:4].values
        
        pos_out = self.atoms_ase.get_scaled_positions(wrap=True)
        Vi_out = self.atoms_ase.get_velocities()
        print("pos (scaled) ", pos_out[0,:], "Vi (Bohr/fs)", Vi_out[0,:]/Bohr*fs)
        
        print("\n units of following data are t:fs, E:eV, T:K, xyz:Ang, Fxyz:eV/Ang, Vi:Ang/?s (?s = 1e-5 *sqrt(u['_e']/u['_amu'])")
        sys.stdout.flush()

        # cpu2gpu
        self.atoms_gpu.set_from_aseAtoms(self.atoms_ase, fixmask)

        return 

    #===========================================================================
    def NVT_berendsen_fix(self, f_atoms, f_initial, \
            b_restart=pm.b_restart, f_restart=pm.f_restart, savefile=pm.savefile, \
            dt=pm.dt, steps=pm.steps, temp0=pm.temp0, tempEnd=pm.tempEnd,\
            taut=pm.taut, \
            n_print=pm.n_print, f_traj=pm.f_traj):
        """
        # NN force NAGETIVE from DFT calculation!!! 
        # So, +f should be -f here!!!
        """
        tautscl=dt/taut

        print("=Job start NNMD.NVT at ", time.ctime())
        self.init_structure_fix(f_atoms, f_fix, temp0)

        Ei, f, Etot = self.getEpFp_uc(self.atoms_gpu.numbers, self.atoms_gpu.cell, self.atoms_gpu.positions)
        #print("first structure mae(df_Fi, f))", mae(df_Fi, f))

        v = self.atoms_gpu.velocities
        r = self.atoms_gpu.get_positions(wrap=False)
        for istep in range(steps):
            self.atoms_gpu.scale_velocities(dt, taut, tempEnd)
            v = self.atoms_gpu.velocities
    
            v += 0.5 * dt * (-f) / self.atoms_gpu.masses *eV/Ang *self.atoms_gpu.fixmask
            r += dt * v

            Ei, f, Etot = self.getEpFp_uc(self.atoms_gpu.numbers, self.atoms_gpu.cell, r)
        
            v += 0.5 * dt * (-f) / self.atoms_gpu.masses *eV/Ang *self.atoms_gpu.fixmask
            
            self.atoms_gpu.positions  = r
            self.atoms_gpu.velocities = v
            self.md_log(dt, istep, Ei, f, Etot, n_print, f_traj)

        print("=Job end NNMD.NVT at ", time.ctime())

    #===========================================================================
    def md_log(self, dt, istep, Ei, f, Etot, n_print=1, f_traj='traj_nnMD.csv'):
        #pos_out = self.atoms_ase.get_positions(wrap=True)
        pos_out = self.atoms_gpu.get_positions(wrap=False)
        Vi_out  = self.atoms_gpu.velocities
        Ekin    = self.atoms_gpu.get_kinetic_energy()
        temp    = 2*Ekin /(self.natoms*3 *kB)

        if istep %n_print == 0:
            print("t_fs: ", "{:4.2f}".format(dt*istep/fs),\
                    "Etot_NN", (Etot),\
                    "+Ek", ( Etot +Ekin ),\
                    "Temp", ( temp ),\
                    " | at ", time.ctime())
            #print("t_fs: ", "{:4.2f}".format(dt*istep/fs),\
            #        "Etot_NN", "{:8.4f}".format(Etot),\
            #        "+Ek", "{:8.4f}".format( Etot +Ekin ),\
            #        "Temp", "{:8.4f}".format( temp ),\
            #        " | at ", time.ctime())
            print("pos: ", pos_out[0,:], " fors ", -f[0,:], "Vi ", Vi_out[0,:])
            sys.stdout.flush()
            
        if f_traj != None:
            if pm.cupyMD:
                Ei      = cp.asnumpy(Ei)
                pos_out = cp.asnumpy(pos_out)
                f       = cp.asnumpy(f)
                Vi_out  = cp.asnumpy(Vi_out)

            #out = np.concatenate([Ei, pos_out, -f, Vi_out], axis=1)
            out = np.concatenate([Ei.T, pos_out.T], axis=0)
            df_out = pd.DataFrame(out)
            df_out.to_csv(f_traj, mode='a', header=False, sep='\t', float_format='%.3f')

            if istep %500 == 0:
                if pm.cupyMD:
                    Ei      = cp.asnumpy(Ei)
                    pos_out = cp.asnumpy(pos_out)
                    f       = cp.asnumpy(f)
                    Vi_out  = cp.asnumpy(Vi_out)

                out = np.concatenate([Ei, pos_out, -f, Vi_out], axis=1)
                df_out = pd.DataFrame(out)
                df_out.to_csv(f_traj+'_restart', mode='a', header=False, sep='\t')

        return
        
    #===========================================================================
    def get_potential_energy(self): 
        # for NN
        _, Ep_tot = self.getEp_uc(self.atoms_gpu.numbers, self.atoms_gpu.cell, self.atoms_gpu.positions)
        # for lmp
        #_, _, Ep_tot = self.getEpFp_uc(self.atoms_gpu.numbers, self.atoms_gpu.cell, self.atoms_gpu.positions)
        
        return Ep_tot

    #===========================================================================
    def calculate_numerical_stress(self, atoms_gpu, d=1e-6, voigt=True, i_xyz=[0, 1, 2]): 
        """Calculate numerical stress using finite difference."""

        e0 = self.get_potential_energy()

        stress = cp.zeros((3, 3), dtype=float)

        cell = atoms_gpu.cell.copy()
        V = atoms_gpu.get_volume()
        # print('d, get_volume', d, V)
        #for i in range(3):
        for i in i_xyz:
            x = cp.eye(3)
            x[i, i] += d
            atoms_gpu.set_cell(cp.dot(cell, x), scale_atoms=True)
            eplus = self.get_potential_energy()

            x[i, i] -= 2 * d
            atoms_gpu.set_cell(cp.dot(cell, x), scale_atoms=True)
            eminus = self.get_potential_energy()

            stress[i, i] = (eplus - eminus) / (2 * d * V)
            print('E0, E+, E-, d, stress(eV), P(eV/A**3)', e0, eplus, eminus, d, stress[i, i]*V, stress[i, i])
            # x[i, i] += d

            # j = i - 2
            # x[i, j] = d
            # x[j, i] = d
            # atoms.set_cell(np.dot(cell, x), scale_atoms=True)
            # eplus = atoms.get_potential_energy(force_consistent=True)

            # x[i, j] = -d
            # x[j, i] = -d
            # atoms.set_cell(np.dot(cell, x), scale_atoms=True)
            # eminus = atoms.get_potential_energy(force_consistent=True)

            # stress[i, j] = (eplus - eminus) / (4 * d * V)
            # stress[j, i] = stress[i, j]
        atoms_gpu.set_cell(cell, scale_atoms=True)

        return stress

    #===========================================================================
    def NVT_berendsen(self, f_atoms, f_initial, \
            b_restart=pm.b_restart, f_restart=pm.f_restart, savefile=pm.savefile, \
            dt=pm.dt, steps=pm.steps, temp0=pm.temp0, tempEnd=pm.tempEnd,\
            taut=pm.taut, \
            n_print=pm.n_print, f_traj=pm.f_traj):
        """
        # NN force NAGETIVE from DFT calculation!!! 
        # So, +f should be -f here!!!
        """
        tautscl=dt/taut

        print("=Job start NNMD.NVT at ", time.ctime())
        df_Fi = self.init_structure(f_atoms, f_initial, b_restart=b_restart, f_restart=f_restart)

        Ei, f, Etot = self.getEpFp_uc(self.atoms_gpu.numbers, self.atoms_gpu.cell, self.atoms_gpu.positions)
        print("first structure mae(df_Fi, f))", mae(df_Fi, f))

        v = self.atoms_gpu.velocities
        r = self.atoms_gpu.get_positions(wrap=False)
        for istep in range(steps):
            self.atoms_gpu.scale_velocities(dt, taut, tempEnd)
            v = self.atoms_gpu.velocities
    
            v += 0.5 * dt * (-f) / self.atoms_gpu.masses *eV/Ang
            r += dt * v

            Ei, f, Etot = self.getEpFp_uc(self.atoms_gpu.numbers, self.atoms_gpu.cell, r)
        
            v += 0.5 * dt * (-f) / self.atoms_gpu.masses *eV/Ang
            
            self.atoms_gpu.positions  = r
            self.atoms_gpu.velocities = v
            self.md_log(dt, istep, Ei, f, Etot, n_print, f_traj)

        print("=Job end NNMD.NVT at ", time.ctime())

    #===========================================================================
    def NPT_berendsen(self, f_atoms, f_initial, \
            b_restart=pm.b_restart, f_restart=pm.f_restart, savefile=pm.savefile, \
            dt=pm.dt, steps=pm.steps, temp0=pm.temp0, tempEnd=pm.tempEnd,\
            taut=pm.taut, taup=pm.taup, stressEnd=pm.stressEnd, n_scl_cell=pm.n_scl_cell, \
            n_print=pm.n_print, f_traj=pm.f_traj):
        """
        # BERENDSEN algrithm
        # NN force NAGETIVE from DFT calculation!!! 
        # So, +f should be -f here!!!
        """
        print("=Job start NNMD.NVT at ", time.ctime())
        df_Fi = self.init_structure(f_atoms, f_initial, b_restart=b_restart, f_restart=f_restart)

        Ei, f, Etot = self.getEpFp_uc(self.atoms_gpu.numbers, self.atoms_gpu.cell, self.atoms_gpu.positions)
        print("first structure mae(df_Fi, f))", mae(df_Fi, f))

        v = self.atoms_gpu.velocities
        r = self.atoms_gpu.get_positions(wrap=False)
        for istep in range(steps):
            self.atoms_gpu.scale_velocities(dt, taut, tempEnd)
            if istep %n_scl_cell == 0:
                #stressNow = self.calculate_numerical_stress(self.atoms_gpu, d=1e-6, voigt=True, i_xyz=[0, 1, 2])
                stressNow = self.calculate_numerical_stress(self.atoms_gpu, d=1e-4, voigt=False, i_xyz=[2])
                #self.atoms_gpu.scale_positions_and_cell(dt, taup, stressEnd, stressNow)
                #self.atoms_gpu.scale_positions_and_cell_xyz(dt, taup, stressEnd, stressNow, i_xyz=[2])
                self.atoms_gpu.scale_positions_and_cell_xyz_PWmat7(dt, taup, stressEnd, stressNow, n_scl_cell, i_xyz=[2])

            v = self.atoms_gpu.velocities
            r = self.atoms_gpu.get_positions(wrap=False)
    
            v += 0.5 * dt * (-f) / self.atoms_gpu.masses *eV/Ang
            r += dt * v

            Ei, f, Etot = self.getEpFp_uc(self.atoms_gpu.numbers, self.atoms_gpu.cell, r)
        
            v += 0.5 * dt * (-f) / self.atoms_gpu.masses *eV/Ang
            
            self.atoms_gpu.positions  = r
            self.atoms_gpu.velocities = v
            self.md_log(dt, istep, Ei, f, Etot, n_print, f_traj)

        print("=Job end NNMD.NVT at ", time.ctime())

    #===========================================================================
