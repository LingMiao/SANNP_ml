#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    References
    ----------
Created on Fri Jan  5 16:39:55 2018
@author: yufeng
Modified on Fri Aug 24 23:29:29 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

import os
import numpy as np
import tensorflow as tf

import parameters as pm
from func_struct import get_atoms_type

if pm.tf_dtype == 'float32' :
    tf_dtype = tf.float32
    print('info: tf.dtype = tf.float32 in Tensorflow trainning.')
else:
    tf_dtype = tf.float64
    print('info: tf.dtype = tf.float64 in Tensorflow trainning, it may be slower.')

#===============================================================================
class EiNN:
    """
    Ei Neural Network (EiNN)

    parameters:

    """

    def __init__(self, \
            nodes_In=pm.nFeats, nodes_L1=pm.nNodeL1, nodes_L2=pm.nNodeL2, \
            maxNb=pm.maxNb, f_atoms=pm.f_atoms):

        self.nIn = nodes_In
        self.nL1 = nodes_L1
        self.nL2 = nodes_L2
        self.nInMax = nodes_In.max()
        self.maxNb  = maxNb

        self.itp_uc, self.at_types, self.at_types_idx = get_atoms_type(f_atoms)
        assert np.all(self.at_types==pm.at_types), "please check at_types in parameter.py, it should be as above value!"
        self.natoms = np.sum(self.at_types[:,1]) 
        self.ntypes = self.at_types.shape[0]

        for itp in range(self.ntypes):
            with tf.variable_scope('NN', reuse=tf.AUTO_REUSE):
                W1 = tf.get_variable("weights1_itp"+str(itp), shape=[self.nIn[itp],self.nL1[itp]],\
                      dtype=tf_dtype, initializer=tf.contrib.layers.xavier_initializer())
                B1 = tf.get_variable("biases1_itp"+str(itp),shape=[self.nL1[itp]],\
                     dtype=tf_dtype, initializer=tf.zeros_initializer())
                
                W2 = tf.get_variable("weights2_itp"+str(itp), shape=[self.nL1[itp],self.nL2[itp]],\
                      dtype=tf_dtype, initializer=tf.contrib.layers.xavier_initializer())
                B2 = tf.get_variable("biases2_itp"+str(itp),shape=[self.nL2[itp]],\
                     dtype=tf_dtype, initializer=tf.zeros_initializer())
                
                W3 = tf.get_variable("weights3_itp"+str(itp), shape=[self.nL2[itp],1],\
                      dtype=tf_dtype, initializer=tf.contrib.layers.xavier_initializer())
                B3 = tf.get_variable("biases3_itp"+str(itp),shape=[1],\
                     dtype=tf_dtype, initializer=tf.zeros_initializer())

                #print("NN.init, W3, B3\n", W3, '\n', B3)

    def _preprocessor(self, features):
        return 

    def getEi_itp(self, itp, features):
        with tf.variable_scope('NN', reuse=tf.AUTO_REUSE):
            W1 = tf.get_variable("weights1_itp"+str(itp), dtype=tf_dtype)
            B1 = tf.get_variable("biases1_itp"+str(itp), dtype=tf_dtype)
            #L1out = tf.nn.sigmoid(tf.matmul(features, W1)+B1)
            L1out = tf.nn.sigmoid(tf.matmul(features[:,:self.nIn[itp]], W1)+B1)
            
            W2 = tf.get_variable("weights2_itp"+str(itp), dtype=tf_dtype)
            B2 = tf.get_variable("biases2_itp"+str(itp), dtype=tf_dtype)
            L2out = tf.nn.sigmoid(tf.matmul(L1out, W2)+B2)
            
            W3 = tf.get_variable("weights3_itp"+str(itp), dtype=tf_dtype)
            B3 = tf.get_variable("biases3_itp"+str(itp), dtype=tf_dtype)
            L3out = tf.matmul(L2out, W3)+B3
    
            #print("NN.getEi_itp, L3out", L3out)
        return L3out

    def getEi(self, features, itypes):
        """
        """
        #print('\n\n getEi()===')
        #print("itypes", itypes)
        itypes = tf.reshape(itypes, [-1,1])
        #print("itypes", itypes)

        Ei = tf.zeros_like(itypes, dtype=tf_dtype)
        #print('Ei', Ei)
        for i in range(self.ntypes):
            idx = tf.equal(itypes[:,0], self.at_types[i, 0])
            #print('idx', idx)
            iEi = self.getEi_itp(i, tf.boolean_mask(features, idx))
            #print('iEi', iEi)
            Ei += tf.scatter_nd(tf.where(idx), iEi, tf.shape(Ei,out_type=tf.int64))

        return Ei
    
    def tf_get_dEldXi(self, itp, features):
        with tf.variable_scope('NN', reuse=tf.AUTO_REUSE):
            W1 = tf.get_variable("weights1_itp"+str(itp), dtype=tf_dtype)
            B1 = tf.get_variable("biases1_itp"+str(itp), dtype=tf_dtype)
            #L1out = tf.nn.sigmoid(tf.matmul(features, W1)+B1)
            L1out = tf.nn.sigmoid(tf.matmul(features[:,:self.nIn[itp]], W1)+B1)
            
            W2 = tf.get_variable("weights2_itp"+str(itp), dtype=tf_dtype)
            B2 = tf.get_variable("biases2_itp"+str(itp), dtype=tf_dtype)
            L2out = tf.nn.sigmoid(tf.matmul(L1out, W2)+B2)
            
            W3 = tf.get_variable("weights3_itp"+str(itp), dtype=tf_dtype)
            B3 = tf.get_variable("biases3_itp"+str(itp), dtype=tf_dtype)
            #print("NN.tf_get_dEldXi, W3, B3\n", W3, '\n', B3)
            
        w_j = tf.reduce_sum(tf.expand_dims(L2out*(1-L2out),1) * \
                           tf.expand_dims(W2,0) * \
                           tf.expand_dims(tf.transpose(W3),0), axis=2)
        dEldXi = tf.reduce_sum(tf.expand_dims(L1out*(1-L1out)*w_j,1) * tf.expand_dims(W1,0),2)

        #o_zeros = tf.zeros((self.at_types[itp,1], self.nInMax -self.nIn[itp]))
        o_zeros = tf.zeros((tf.shape(features)[0], self.nInMax -self.nIn[itp]), dtype=tf_dtype)
        #print("NN.tf_get_dEldXi, o_zeros", o_zeros)
        tf_dEldXi = tf.concat([dEldXi,o_zeros], axis=1)
        #print("NN.tf_get_dEldXi, tf_dEldXi", tf_dEldXi)
    
        return tf_dEldXi

    def getFi(self, features, tfdXi, tfdXin, tf_idxNb):
        """
        defult nImg=1
        """
        #print('\n\n getFi()===')

        tf_dEldXi = tf.zeros_like(features, dtype=tf_dtype)
        #print('tf_dEldXi', tf_dEldXi)
        for i in range(self.ntypes):
            idx = tf.equal(self.itp_uc, self.at_types[i, 0])
            #print('idx', idx)
            i_dEldXi = self.tf_get_dEldXi(i, tf.boolean_mask(features, idx))
            #print('i_dEldXi', i_dEldXi)
            tf_dEldXi += tf.scatter_nd(tf.where(idx), i_dEldXi, tf.shape(tf_dEldXi,out_type=tf.int64))

        #tf_dEldXi = self.tf_get_dEldXi(itype, features)
        Fll = tf.reduce_sum(tf.expand_dims(tf_dEldXi,2)*tfdXi,axis=1)
        #print("getFi().F11 \n", Fll )
            
        dENldXi  = tf.gather_nd(tf_dEldXi, \
                tf.expand_dims(tf.transpose(tf.boolean_mask(tf_idxNb, tf.greater(tf_idxNb,0))-1),1))
        dEnldXin = tf.scatter_nd(tf.where(tf.greater(tf_idxNb,0)), \
                dENldXi, (self.natoms, self.maxNb, self.nInMax))
            
        #print("getFi().dENldXi \n",
        #       dENldXi )
        #print("getFi().dEnldXin \n",
        #        dEnldXin )

        Fln = tf.reduce_sum(tf.expand_dims(dEnldXin,3)*tfdXin,axis=[1,2])
        #print("getFi().Fln \n", Fln)

        return Fll + Fln

    def saveWij_np(self, sess, f_npfile):
        nnWij = []
        for itp in range(self.ntypes):
            with tf.variable_scope('NN', reuse=tf.AUTO_REUSE):
                W1 = tf.get_variable("weights1_itp"+str(itp), dtype=tf_dtype)
                B1 = tf.get_variable("biases1_itp"+str(itp), dtype=tf_dtype)
                W2 = tf.get_variable("weights2_itp"+str(itp), dtype=tf_dtype)
                B2 = tf.get_variable("biases2_itp"+str(itp), dtype=tf_dtype)
                W3 = tf.get_variable("weights3_itp"+str(itp), dtype=tf_dtype)
                B3 = tf.get_variable("biases3_itp"+str(itp), dtype=tf_dtype)

                nnWij.append(np.array(W1.eval(session=sess)))
                nnWij.append(np.array(W2.eval(session=sess)))
                nnWij.append(np.array(W3.eval(session=sess)))
                nnWij.append(np.array(B1.eval(session=sess)))
                nnWij.append(np.array(B2.eval(session=sess)))
                nnWij.append(np.array(B3.eval(session=sess)))

        nnWij = np.array(nnWij)
        np.save(f_npfile, nnWij)
        print('EiNN.saveWij_np to', f_npfile, nnWij.dtype, nnWij.shape)
        return

    def loadWij_np_check(self, sess, f_npfile):
        nnWij = np.load(f_npfile)
        print('EiNN.loadWij_np_check from', f_npfile, nnWij.dtype, nnWij.shape)

        err = 0
        for itp in range(self.ntypes):
            with tf.variable_scope('NN', reuse=tf.AUTO_REUSE):
                W1 = tf.get_variable("weights1_itp"+str(itp), dtype=tf_dtype)
                B1 = tf.get_variable("biases1_itp"+str(itp), dtype=tf_dtype)
                W2 = tf.get_variable("weights2_itp"+str(itp), dtype=tf_dtype)
                B2 = tf.get_variable("biases2_itp"+str(itp), dtype=tf_dtype)
                W3 = tf.get_variable("weights3_itp"+str(itp), dtype=tf_dtype)
                B3 = tf.get_variable("biases3_itp"+str(itp), dtype=tf_dtype)

                err += np.sum(nnWij[itp*6+0] -np.array(W1.eval(session=sess)))
                err += np.sum(nnWij[itp*6+1] -np.array(W2.eval(session=sess)))
                err += np.sum(nnWij[itp*6+2] -np.array(W3.eval(session=sess)))
                err += np.sum(nnWij[itp*6+3] -np.array(B1.eval(session=sess)))
                err += np.sum(nnWij[itp*6+4] -np.array(B2.eval(session=sess)))
                err += np.sum(nnWij[itp*6+5] -np.array(B3.eval(session=sess)))

                sess.run(W1.assign(nnWij[itp*6+0]))
                sess.run(W2.assign(nnWij[itp*6+1]))
                sess.run(W3.assign(nnWij[itp*6+2]))
                sess.run(B1.assign(nnWij[itp*6+3]))
                sess.run(B2.assign(nnWij[itp*6+4]))
                sess.run(B3.assign(nnWij[itp*6+5]))

        print('sum(diff_Wij_Bi) =', err)
        return 


#===============================================================================
class NNapiBase:
    """
    NNapiBase

    parameters:

    """

    #===========================================================================
    def __init__(self, nn, data_scaler):

        self.maxNb  = nn.maxNb
        self.natoms = nn.natoms
        self.itp_uc = nn.itp_uc

        # === input layer ===
        self.X   = tf.placeholder(tf_dtype, shape=(None, nn.nInMax),name="X")
        self.itp = tf.placeholder(tf.int64, shape=(None), name="itypes")
        self.YEi = tf.placeholder(tf_dtype, shape=(None,1),name="YEi")

        self.tfdXi    = tf.placeholder(tf_dtype, shape=(None, nn.nInMax,3))
        self.tfdXin   = tf.placeholder(tf_dtype, shape=(None, nn.maxNb,nn.nInMax,3))
        self.tf_idxNb = tf.placeholder(tf.int64, shape=(None, nn.maxNb))
        self.YFi = tf.placeholder(tf_dtype, shape=(None,3),name="YFi")

        # === output layer ===
        self.pred_Ei = nn.getEi(self.X, self.itp)
        self.pred_Fi = nn.getFi(self.X, self.tfdXi, self.tfdXin, self.tf_idxNb)

        # === data scaler process  ===
        self.ds = data_scaler

    #===========================================================================
    def init_sess(self, nn_file):
        config = tf.ConfigProto(allow_soft_placement=True)
        config.gpu_options.per_process_gpu_memory_fraction = pm.gpu_mem
        self.sess = tf.Session(config=config)

        self.saver = tf.train.Saver()
        print("\ninit_sess(), NN expect to be restored from ", nn_file)
        if os.path.exists(nn_file +".index" ):
            self.saver.restore(self.sess, nn_file)
            print("NN is restored succesfully") 
        else:
            print("Warning! There is NO NN_model file ", nn_file)
            print("Will init a new NN ...")
            self.sess.run(tf.global_variables_initializer())

        return self.sess

    def get_sess(self):
        return self.sess
