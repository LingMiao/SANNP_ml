#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Process data io
External APIs: 
    r_atom_loc_struct()
    r_feat_csv()
    gen_feats_rndImg()

Created on Wed Aug 22 10:58:18 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

import numpy as np
import pandas as pd

import os
import time
import sys

import parameters as pm

#================================================================================
'''
These mdtraj_* for read and write NNMD traj file
'''
#================================================================================
class mdtraj_EiP:
    """
    parameters:
    """

    def __init__(self, cell=None, itype=None):
        self.df_chunk  = None
        return

    def open4read(self, f_traj, natoms, img_skip=1):
        self.df_chunk = pd.read_csv(f_traj, header=None, index_col=False, dtype="float", \
                iterator=True, chunksize=4*img_skip, delim_whitespace=True)

        self.lines = 4
        return

    def r_EiPFV_itr(self):
        df = self.df_chunk.get_chunk()

        Ei   =(df.iloc[0, 1:])[:, np.newaxis]
        pos  = df.iloc[1:4, 1:].values.T
        return Ei, pos, pos, pos  # sub for Fi Vi

    def r_EiPFV_all(self):
        print("Sorry, NOT implement now~")
        return False


#================================================================================
class mdtraj_EiPFV:
    """
    parameters:
    """

    def __init__(self, cell=None, itype=None):
        self.df_chunk  = None
        return

    def open4read(self, f_traj, natoms, img_skip=1):
        self.df_chunk = pd.read_csv(f_traj, header=None, index_col=False, dtype="float", \
                iterator=True, chunksize=natoms*img_skip, delim_whitespace=True)

        self.lines = natoms
        return

    def r_EiPFV_itr(self):
        df = self.df_chunk.get_chunk()

        Ei   =(df.iloc[:self.lines, 1])[:,np.newaxis]
        pos  = df.iloc[:self.lines, 2:5].values
        Fi   = df.iloc[:self.lines, 5:8].values
        Vi   = df.iloc[:self.lines, 8:11].values
        return Ei, pos, Fi, Vi

    def r_EiPFV_all(self):
        print("Sorry, NOT implement now~")
        return False
#================================================================================

#================================================================================
'''
These MVMT_* for read and write PWmat MOVEMENT file
'''
#================================================================================
class MVMT_EiPFV:
    """
    parameters:
    """

    def __init__(self, cell=None, itype=None):
        self.df_chunk  = None
        self.cell      = cell  # 3x3 Ang
        self.itype     = itype # nx1 Ang
        return

    def open4read(self, f_traj, natoms, img_skip=1):
        self.df_chunk = pd.read_csv(f_traj, header=None, index_col=False, dtype="float", \
                iterator=True, chunksize=4*natoms*img_skip, delim_whitespace=True)

        self.lines = natoms
        return

    def r_EiPFV_itr(self):
        df = self.df_chunk.get_chunk()

        natoms = self.lines
        #itype = df[0:natoms,0]
        pos  = np.matmul(df.iloc[0:natoms, 1:4], self.cell.T)
        Fi   = df.iloc[natoms*1:natoms*2, 1:4]
        Vi   = df.iloc[natoms*2:natoms*3, 1:4]
        Ei   =(df.iloc[natoms*3:natoms*4, 1])[:,np.newaxis]

        return Ei, pos, Fi, Vi

    def r_EiPFV_all(self):
        print("Sorry, NOT implement now~")
        return False
#================================================================================
