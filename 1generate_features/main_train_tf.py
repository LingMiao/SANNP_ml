#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Modified on Thu Aug 23 23:37:47 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

import numpy as np
import tensorflow as tf
import os

import parameters as pm
from func_data_scaler import DataScaler
from nn_model import EiNN
from nn_train import Trainer
from nn_md    import NNMD
from func_io import r_feat_csv

os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
os.environ["CUDA_VISIBLE_DEVICES"] = pm.cuda_dev
os.system('cat parameters.py')
#=======================================================================
with tf.device('/device:GPU:3'):
    nn = EiNN()
    data_scaler = DataScaler(f_ds=pm.f_data_scaler, f_feat=pm.f_pretr_feat)

    trainer = Trainer(nn, data_scaler)
    sess = trainer.init_sess(pm.f_Einn_model)
    #nn.loadWij_np_check(sess, pm.f_Wij_np)

    itypes,feat,engy = r_feat_csv(pm.f_train_feat)
    #sess = trainer.init_sess(pm.f_Einn_model)
    #trainer.train_Ei(pm.f_pretr_feat, pm.f_test_feat, pm.epochs_pretrain, nn_file=pm.d_nnEi+'preEi', eMAE_err=pm.eMAE_err)
    #trainer.train_Ei(pm.f_train_feat, pm.f_test_feat, pm.epochs_alltrain, nn_file=pm.d_nnEi+'allEi', eMAE_err=pm.eMAE_err)
    nn.saveWij_np(sess, pm.f_Wij_np+'_Ei')
    trainer.getEi_err(itypes,feat,engy, outfile=pm.f_train_feat+'_Ei_ckEi')
    trainer.getFi_err(pm.f_train_nblt, outfile=pm.f_train_feat+'_Ei_ckFi')

    trainer.train_Fi(pm.f_train_nblt, pm.f_test_nblt, pm.epochs_Fi_train, nn_file=pm.d_nnFi+'Fi', iFi_repeat=pm.iFi_repeat)
    nn.saveWij_np(sess, pm.f_Wij_np)
    trainer.getEi_err(itypes,feat,engy, outfile=pm.f_train_feat+'_Fi_ckEi')
    trainer.getFi_err(pm.f_train_nblt, outfile=pm.f_train_feat+'_Fi_ckFi')

    # nnmd = NNMD(nn, data_scaler)
    # sess = nnmd.init_sess(pm.f_Finn_model)
    # nn.loadWij_np_check(sess, pm.f_Wij_np)

    # nnmd.NVE(pm.f_atoms, pm.f_initial, b_restart=False, dt=pm.dt, steps=pm.steps, f_traj=None)
    # nnmd.NVT(pm.f_atoms, pm.f_initial, tempEnd=100)
