#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
External APIs: 
    get_feat_dfeat_* functions

Version 1.1
    use cos_log todescribe features

Modified on Wed Aug 22 10:54:13 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
Created on Tue Dec  5 16:08:54 2017
@author: yufeng

llp: linear picewise polynomials
"""

import parameters as pm
if pm.cupyFeat:
    import cupy as cp
else:
    import numpy as cp

import numpy as np
import matplotlib
#matplotlib.use('agg')
import matplotlib.pyplot as plt

import time

#================================================================================
""" basic functions to describe the features
see test code at end of this file
use cos_log todescribe features
2018.6.10 Morning
"""

#@profile
def getCos(x, nBasis):
    yLPP = (cp.logspace(cp.log10(1),cp.log10(9),nBasis+2)-5)/4
    h = (yLPP[1:] - yLPP[:-1])*2
    #print(yLPP)
    #print(h)

    xy0 =  x[:,cp.newaxis] - yLPP[1:-1]
    #print(xy0)
    xy =  cp.zeros((x.shape[0], nBasis))
    h0 = h[cp.newaxis,:-1] -xy
    mask = cp.where( cp.abs(xy0)<h0 )
    xy[mask] = cp.cos(xy0[mask]/h0[mask]*cp.pi)/2 +0.5
    
    return xy

def getCos_log_debug(x, nBasis):
    """ for cmp with above getCos func
    """
    yLPP = (cp.logspace(cp.log10(1),cp.log10(9),nBasis+2)-5)/4
    h = (yLPP[1:] - yLPP[:-1])*2
    #print(yLPP)
    #print(h)

    xy =  x[:,cp.newaxis] - yLPP[1:-1]
    for i in range(xy.shape[1]):
        xy[:,i][xy[:,i]>h[i]]  = 1 *h[i]
        xy[:,i][xy[:,i]<-h[i]] = 1 *h[i]
        xy[:,i] = cp.cos(xy[:,i]/h[i]*cp.pi)/2 +0.5
    #print(xy)
    
    return xy

##@profile
def getdCos(x, nBasis):
    """ derivative of feature function 
    """
    yLPP = (cp.logspace(cp.log10(1),cp.log10(9),nBasis+2)-5)/4
    h = (yLPP[1:] - yLPP[:-1])*2
    
    xy0 =  x[:,cp.newaxis] - yLPP[1:-1]
    xy =  cp.zeros((x.shape[0], nBasis))
    h0 = h[cp.newaxis,:-1] -xy
    mask = cp.where( cp.abs(xy0)<h0 )
    #xy[mask] = cp.cos(xy0[mask]/h0[mask]*cp.pi)/2 +0.5
    xy[mask] = -cp.sin(xy0[mask]/h0[mask]*cp.pi)*cp.pi/h0[mask]/2
    
    return xy

def getdCos_log_debug(x, nBasis):
    yLPP = (cp.logspace(cp.log10(1),cp.log10(9),nBasis+2)-5)/4
    h = (yLPP[1:] - yLPP[:-1])*2
    
    xy =  x[:,cp.newaxis] - yLPP[1:-1]
    for i in range(xy.shape[1]):
        xy[:,i][xy[:,i]>h[i]]  = 0
        xy[:,i][xy[:,i]<-h[i]] = 0
        xy[:,i] = -cp.sin(xy[:,i]/h[i]*cp.pi)*cp.pi/h[i]/2
    
    return xy

#=================================================
""" old version from yufeng
"""
def getCos_yf(x, nBasis):
    yLPP = cp.linspace(-1,1,nBasis)
    h = yLPP[1:] - yLPP[:-1]
    
    xy =  x[:,cp.newaxis] - yLPP
    zeroMask = (xy==0)
    xy[xy>cp.concatenate((h, [0]))] = 0
    xy[xy<-cp.concatenate(([0],h))] = 0
    
    xyR = xy[:,:-1]
    xyL = xy[:,1:]
    
    (xy[:,:-1])[xyR>0] = cp.cos((xyR/h)[xyR>0]*cp.pi)/2+0.5
    (xy[:,1:])[xyL<0] = cp.cos((xyL/h)[xyL<0]*cp.pi)/2+0.5
    xy[zeroMask]=1
    return xy

def getdCos_yf(x, nBasis):
    yLPP = cp.linspace(-1,1,nBasis)
    h = yLPP[1:] - yLPP[:-1]
    
    xy =  x[:,cp.newaxis] - yLPP
    xy[xy>cp.concatenate((h, [0]))] = 0
    xy[xy<-cp.concatenate(([0],h))] = 0
    
    xyR = xy[:,:-1]
    xyL = xy[:,1:]
    
    (xy[:,:-1])[xyR>0] = cp.sin((xyR/h)[xyR>0]*cp.pi)
    (xy[:,1:])[xyL<0] = cp.sin((xyL/h)[xyL<0]*cp.pi)
    (xy[:,:-1])[xyR>0] = (0.5*cp.pi*xy[:,:-1]/h)[xyR>0]
    (xy[:,1:])[xyL<0] = (0.5*cp.pi*xy[:,1:]/h)[xyL<0]
    return -xy

# === 2018.5.9 Morning
""" Simplified version with improved efficiency
"""
def getCos_ml(x, nBasis):
    yLPP = cp.linspace(-1,1,nBasis+2)
    h = yLPP[1] - yLPP[0]
    
    xy =  x[:,cp.newaxis] - yLPP[1:-1]
    xy[xy>h] = 1 *h
    xy[xy<-h] = 1 *h
    
    xy = cp.cos(xy/h*cp.pi)/2 +0.5
    return xy

def getdCos_ml(x, nBasis):
    yLPP = cp.linspace(-1,1,nBasis+2)
    h = yLPP[1] - yLPP[0]
    
    xy =  x[:,cp.newaxis] - yLPP[1:-1]
    xy[xy>h] = 0
    xy[xy<-h] = 0
    
    xy = cp.sin(xy/h*cp.pi)*cp.pi/h/2
    return -xy

#================================================================================
def get_loc_dis(nbxyz):
    """ get local distance matrix from local neighbor xyz
    """
    Ri = cp.sqrt(cp.sum(nbxyz**2,axis=2))
    Dc = cp.sqrt(cp.sum((nbxyz[:,:,cp.newaxis,:]-nbxyz[:,cp.newaxis,:,:])**2,axis=3))
    Dc[Ri==0] = 0
    Dc.transpose([0,2,1])[Ri==0] = 0
    Rhat = cp.zeros_like(nbxyz)
    Rhat = nbxyz/Ri[:,:,cp.newaxis]
    Rhat[Ri==0] = 0
    #print("Rhat.shape, Ri.shape, Dc.shape", Rhat.shape, Ri.shape, Dc.shape)
    return Rhat, Ri, Dc

#================================================================================
#@profile
def getFeatures(Ri, Dc,nLPP2b,nLPP3b):
    """ generate features from local distance matrix
    # Ri.shape (n-D) and Ri[Ri>0].shape (1-D)
    # actrually, getCos(Ri<R0 or Ri>Rc) = 0 automatically!!!
    """
    RcA = 2/(pm.Rc-pm.R0)
    RcB = (pm.Rc+pm.R0)/(pm.Rc-pm.R0)

    yR_lpp = cp.zeros((Ri.shape[0], Ri.shape[1], nLPP2b))
    yR_lpp[Ri>0] = getCos(Ri[Ri>0]*RcA -RcB, nLPP2b)
    yR = yR_lpp.sum(axis=1)

    yDR = cp.zeros((Ri.shape[0], Ri.shape[1],nLPP3b))
    yDc = cp.zeros((Dc.shape[0], Dc.shape[1], Dc.shape[2],nLPP3b))
    yDR[Ri>0] = getCos(Ri[Ri>0]*RcA -RcB, nLPP3b)
    yDc[Dc>0] = getCos(Dc[Dc>0]*RcA -RcB, nLPP3b)

    # _yDc[Dc>0] = getCos_log_debug(Dc[Dc>0]*RcA -RcB, nLPP3b)
    # _yDc = getCos(Dc.reshape((-1))*RcA -RcB, nLPP3b).reshape((Dc.shape[0], Dc.shape[1], Dc.shape[2],nLPP3b))
    # print("check getCos_yDc", cp.sum(cp.abs(yDc - _yDc)))

    yD1 = (yDc[:,:,:,cp.newaxis,:] * yDR[:,cp.newaxis,:,:,cp.newaxis]).sum(axis=2)
    yD2 = (yD1[:,:,cp.newaxis,:,:] * yDR[:,:,:,cp.newaxis,cp.newaxis]).sum(axis=1)
    yDlpp = yD2.reshape([Ri.shape[0],-1])

    return yR, yDlpp, yDR, yDc, yD1# Morning

#@profile
def getDFeatures(Ri,Dc,Rhat, nLPP2b,nLPP3b, yDR,yDc,yD1):
    """ derivative of feature function 
    """
    RcA = 2/(pm.Rc-pm.R0)
    RcB = (pm.Rc+pm.R0)/(pm.Rc-pm.R0)
    
    dyRn = cp.zeros((Ri.shape[0], Ri.shape[1], nLPP2b))
    dyRn[Ri>0] = getdCos(Ri[Ri>0]*RcA -RcB, nLPP2b)*RcA
    dyRn = dyRn[:,:,:,cp.newaxis] * Rhat[:,:,cp.newaxis,:]
    dyR = dyRn.sum(axis=1)  # Morning

    dyD = cp.zeros((Ri.shape[0], Ri.shape[1], nLPP3b))
    dyD[Ri>0] = getdCos(Ri[Ri>0]*RcA -RcB, nLPP3b)*RcA

    dyD_half = (Rhat[:,:,cp.newaxis,cp.newaxis,cp.newaxis,:] * \
            yD1[:,:,cp.newaxis,:,:,cp.newaxis] * \
            dyD[:,:,:,cp.newaxis,cp.newaxis,cp.newaxis]).sum(axis=1)
    dyDcos = (dyD_half.transpose([0,2,1,3,4]) + dyD_half ).reshape([Ri.shape[0],-1,3])
    
    dX2indRl = yD1.transpose([0,1,3,2])[:,:,cp.newaxis,:,:] * dyD[:,:,:,cp.newaxis,cp.newaxis]
    dX2indRl = dX2indRl + dX2indRl.transpose([0,1,3,2,4])
    #_dX2indRl = dX2indRl[:,:,:,:,:,cp.newaxis] * Rhat[:,:,cp.newaxis,cp.newaxis,cp.newaxis,:]
    dX2indRl = (dX2indRl[:,:,cp.newaxis,:,:,:] * Rhat[:,:,:,cp.newaxis,cp.newaxis,cp.newaxis]).transpose([0,1,3,4,5,2])
    
    # time consum here, slow!!!
    ### dX2indRl2 = cp.sum(yDc[:,:,:,:,cp.newaxis,cp.newaxis] * dyD[:,:,cp.newaxis,cp.newaxis,:,cp.newaxis] * Rhat[:,:,cp.newaxis,cp.newaxis,cp.newaxis,:],axis=1)
    #print("yDc.shape, dyD.shape, Rhat.shape \n", \
    #        yDc[:,:,:,:,cp.newaxis,cp.newaxis].shape, \
    #        dyD[:,:,cp.newaxis,cp.newaxis,:,cp.newaxis].shape, \
    #        Rhat[:,:,:,cp.newaxis,cp.newaxis,cp.newaxis].shape)

    dX2indRl0 = dyD[:,:,cp.newaxis,cp.newaxis,:,cp.newaxis] * Rhat[:,:,cp.newaxis,cp.newaxis,cp.newaxis,:]
    dX2indRl0 = dX2indRl0 *yDc[:,:,:,:,cp.newaxis,cp.newaxis]
    dX2indRl2 = cp.sum(dX2indRl0, axis=1)
    dX2indRl2 = dX2indRl2[:,:,cp.newaxis,:,:,:] * yDR[:,:,:,cp.newaxis,cp.newaxis,cp.newaxis]
    dX2indRl2 = dX2indRl2.transpose([0,1,3,2,4,5]) + dX2indRl2
    
    dyDn = (dX2indRl + dX2indRl2).reshape([Ri.shape[0],pm.maxNb,nLPP3b**3,3])
    
    return -dyR, -dyDcos, -dyRn, -dyDn

#=================================================
def getDFeatures_yf(Ri,Dc,Rhat, nLPP2b,nLPP3b):
    """ old version from yufeng
    """
    RcA = 2/(pm.Rc-pm.R0)
    RcB = (pm.Rc+pm.R0)/(pm.Rc-pm.R0)
    dyR = cp.zeros((Ri.shape[0], Ri.shape[1], nLPP2b))
    dyR[Ri>0] = getdCos(Ri[Ri>0]*RcA -RcB, nLPP2b)*RcA
    dyR = (dyR[:,:,:,cp.newaxis]*Rhat[:,:,cp.newaxis,:]).sum(axis=1)
    
    dyRn = cp.zeros((Ri.shape[0], Ri.shape[1], nLPP2b))
    dyRn[Ri>0] = getdCos(Ri[Ri>0]*RcA -RcB, nLPP2b)*RcA
    dyRn = dyRn[:,:,:,cp.newaxis] * Rhat[:,:,cp.newaxis,:]

    yDR = cp.zeros((Ri.shape[0], Ri.shape[1],nLPP3b))
    yDc = cp.zeros((Dc.shape[0], Dc.shape[1], Dc.shape[2],nLPP3b))
    yDR[Ri>0] = getCos(Ri[Ri>0]*RcA -RcB, nLPP3b)
    yDc[Dc>0] = getCos(Dc[Dc>0]*RcA -RcB, nLPP3b)
    yD1 = (yDc[:,:,:,cp.newaxis,:] * yDR[:,cp.newaxis,:,:,cp.newaxis]).sum(axis=2)
    
    dyD = cp.zeros((Ri.shape[0], Ri.shape[1], nLPP3b))
    dyD[Ri>0] = getdCos(Ri[Ri>0]*RcA -RcB, nLPP3b)*RcA
    dyD_half = (Rhat[:,:,cp.newaxis,cp.newaxis,cp.newaxis,:] * \
            yD1[:,:,cp.newaxis,:,:,cp.newaxis] * \
            dyD[:,:,:,cp.newaxis,cp.newaxis,cp.newaxis]).sum(axis=1)
    #dyDcos = dyD_half.transpose([0,2,1,3,4]) + dyD_half
    dyDcos = (dyD_half.transpose([0,2,1,3,4]) + dyD_half ).reshape([Ri.shape[0],-1,3])

    dyDn = cp.zeros((Ri.shape[0], Ri.shape[1], nLPP3b))
    dyDn[Ri>0] = getdCos(Ri[Ri>0]*RcA -RcB, nLPP3b)*RcA
    
    dX2indRl = yD1.transpose([0,1,3,2])[:,:,cp.newaxis,:,:] * dyDn[:,:,:,cp.newaxis,cp.newaxis]
    dX2indRl = dX2indRl + dX2indRl.transpose([0,1,3,2,4])
    dX2indRl = dX2indRl[:,:,:,:,:,cp.newaxis] * Rhat[:,:,cp.newaxis,cp.newaxis,cp.newaxis,:]
    
    dX2indRl2 = cp.sum(yDc[:,:,:,:,cp.newaxis,cp.newaxis] * dyD[:,:,cp.newaxis,cp.newaxis,:,cp.newaxis] * \
                Rhat[:,:,cp.newaxis,cp.newaxis,cp.newaxis,:],axis=1)
    dX2indRl2 = dX2indRl2[:,:,cp.newaxis,:,:,:] * yDR[:,:,:,cp.newaxis,cp.newaxis,cp.newaxis]
    dX2indRl2 = dX2indRl2.transpose([0,1,3,2,4,5]) + dX2indRl2
    
    dyDn = (dX2indRl + dX2indRl2).reshape([Ri.shape[0],pm.maxNb,nLPP3b**3,3])
    
    return -dyR, -dyDcos, -dyRn, -dyDn

#=================================================
def getDFeatures_ml(Ri, Dc, Rhat,nLPP2b,nLPP3b, yDR, yDc, yD1):
    """ Simplified version with improved efficiency
    Morning Thu Jun 14 02:40:42 PDT 2018
    """
    RcA = 2/(pm.Rc-pm.R0)
    RcB = (pm.Rc+pm.R0)/(pm.Rc-pm.R0)
    #dyR = cp.zeros((Ri.shape[0], Ri.shape[1], nLPP2b))
    #dyR[Ri>0] = getdCos(Ri[Ri>0]*RcA -RcB, nLPP2b)*RcA
    #dyR = (dyR[:,:,:,cp.newaxis]*Rhat[:,:,cp.newaxis,:]).sum(axis=1)
    
    dyRn = cp.zeros((Ri.shape[0], Ri.shape[1], nLPP2b))
    dyRn[Ri>0] = getdCos(Ri[Ri>0]*RcA -RcB, nLPP2b)*RcA
    dyRn = dyRn[:,:,:,cp.newaxis] * Rhat[:,:,cp.newaxis,:]
    dyR = dyRn.sum(axis=1)  # Morning

    #yDR = cp.zeros((Ri.shape[0], Ri.shape[1],nLPP3b))
    #yDc = cp.zeros((Dc.shape[0], Dc.shape[1], Dc.shape[2],nLPP3b))
    #yDR[Ri>0] = getCos(Ri[Ri>0]*RcA -RcB, nLPP3b)
    #yDc[Dc>0] = getCos(Dc[Dc>0]*RcA -RcB, nLPP3b)
    #yD1 = (yDc[:,:,:,cp.newaxis,:] * yDR[:,cp.newaxis,:,:,cp.newaxis]).sum(axis=2)
    
    dyD = cp.zeros((Ri.shape[0], Ri.shape[1], nLPP3b))
    dyD[Ri>0] = getdCos(Ri[Ri>0]*RcA -RcB, nLPP3b)*RcA
    dyD_half = (Rhat[:,:,cp.newaxis,cp.newaxis,cp.newaxis,:] * \
            yD1[:,:,cp.newaxis,:,:,cp.newaxis] * \
            dyD[:,:,:,cp.newaxis,cp.newaxis,cp.newaxis]).sum(axis=1)
    dyDcos = (dyD_half.transpose([0,2,1,3,4]) + dyD_half ).reshape([Ri.shape[0],-1,3])

    #dyDn = cp.zeros((Ri.shape[0], Ri.shape[1], nLPP3b))
    #dyDn[Ri>0] = getdCos(Ri[Ri>0]*RcA -RcB, nLPP3b)*RcA
    
    #dX2indRl = yD1.transpose([0,1,3,2])[:,:,cp.newaxis,:,:] * dyDn[:,:,:,cp.newaxis,cp.newaxis]
    dX2indRl = yD1.transpose([0,1,3,2])[:,:,cp.newaxis,:,:] * dyD[:,:,:,cp.newaxis,cp.newaxis]
    dX2indRl = dX2indRl + dX2indRl.transpose([0,1,3,2,4])
    print("dX2indRl, Rhat \n",\
            dX2indRl[:,:,:,:,:,cp.newaxis].shape, \
            Rhat[:,:,cp.newaxis,cp.newaxis,cp.newaxis,:].shape)
    #dX2indRl_ = dX2indRl[:,:,:,:,:,cp.newaxis] * Rhat[:,:,cp.newaxis,cp.newaxis,cp.newaxis,:]
    dX2indRl = (dX2indRl[:,:,cp.newaxis,:,:,:] * Rhat[:,:,:,cp.newaxis,cp.newaxis,cp.newaxis]).transpose([0,1,3,4,5,2])
    
    # time consum here, slow!!!
    ### dX2indRl2 = cp.sum(yDc[:,:,:,:,cp.newaxis,cp.newaxis] * dyD[:,:,cp.newaxis,cp.newaxis,:,cp.newaxis] * Rhat[:,:,cp.newaxis,cp.newaxis,cp.newaxis,:],axis=1)
    print("yDc.shape, dyD.shape, Rhat.shape \n", \
            yDc[:,:,:,:,cp.newaxis,cp.newaxis].shape, \
            dyD[:,:,cp.newaxis,cp.newaxis,:,cp.newaxis].shape, \
            Rhat[:,:,:,cp.newaxis,cp.newaxis,cp.newaxis].shape)

    #dX2indRl0_ = yDc[:,:,:,:,cp.newaxis] * dyD[:,:,cp.newaxis,cp.newaxis,:] 
    dX2indRl0 = (yDc[:,:,cp.newaxis,:,:] * dyD[:,:,:,cp.newaxis,cp.newaxis]).transpose([0,1,3,4,2])
    #dX2indRl0_ = dX2indRl0[:,:,:,:,:,cp.newaxis] * Rhat[:,:,cp.newaxis,cp.newaxis,cp.newaxis,:]
    dX2indRl0 = (dX2indRl0[:,:,cp.newaxis,:,:,:] * Rhat[:,:,:,cp.newaxis,cp.newaxis,cp.newaxis]).transpose([0,1,3,4,5,2])
    dX2indRl2 = cp.sum(dX2indRl0, axis=1)
    print("dX2indRl2.shape, yDR.shape \n", \
            dX2indRl2[:,:,cp.newaxis,:,:,:].shape, \
            yDR[:,:,:,cp.newaxis,cp.newaxis,cp.newaxis].shape)
    dX2indRl2 = dX2indRl2[:,:,cp.newaxis,:,:,:] * yDR[:,:,:,cp.newaxis,cp.newaxis,cp.newaxis]
    dX2indRl2 = dX2indRl2.transpose([0,1,3,2,4,5]) + dX2indRl2
    
    dyDn = (dX2indRl + dX2indRl2).reshape([Ri.shape[0],pm.maxNb,nLPP3b**3,3])
    
    return -dyR, -dyDcos, -dyRn, -dyDn

#================================================================================
# External APIs: get_feat_dfeat_* functions
# Morning 8/22/2018
#================================================================================
def get_feat_nbxyz(itypes, nNb, idxNb, nbxyz):
    """ get feature from local neigbor xyz struture
        Need debug for n-type atoms
    """
    nbxyz = cp.asarray(nbxyz)

    nLPP2b = pm.nLPP2b.max()
    nLPP3b    = pm.nLPP3b.max()

    Rhat,Ri,Dc = get_loc_dis(nbxyz)
    yR, yDlpp, _,_,_ = getFeatures(Ri,Dc,nLPP2b,nLPP3b)
    feat = cp.concatenate([yR, yDlpp], axis=1)
    
    if pm.cupyFeat and (not pm.cupyMD):
        feat = cp.asnumpy(feat)
    return feat

#@profile
def get_feat_dfeat_nbxyz(itypes, nNb, idxNb, nbxyz):
    """ get feature & derivative of feature 
            from local neigbor xyz struture
        Need debug for n-type atoms
    """
    nbxyz = cp.asarray(nbxyz)

    nLPP2b = pm.nLPP2b.max()
    nLPP3b = pm.nLPP3b.max()

    Rhat,Ri,Dc = get_loc_dis(nbxyz)

    yR, yDlpp, yDR, yDc, yD1 = getFeatures(Ri,Dc,nLPP2b,nLPP3b)
    feat = cp.concatenate([yR, yDlpp], axis=1)

    dyR, dyDcos, dyRn, dyDn = \
            getDFeatures(Ri,Dc,Rhat, nLPP2b,nLPP3b, yDR,yDc,yD1)

    dXidRl  = cp.concatenate([dyR, dyDcos], axis=1)
    dXindRl = cp.concatenate([dyRn,dyDn],axis=2)

    '''
    t0 = time.time()

    for i in range(10):
        yR, yDlpp, yDR, yDc, yD1 = getFeatures(Ri,Dc,nLPP2b,nLPP3b)
    feat = cp.concatenate([yR, yDlpp], axis=1)
    t1 = time.time()
    print('time getFeatures',t1-t0)
    t0 = t1
    
    for i in range(10):
        dyR, dyDcos, dyRn, dyDn = \
                getDFeatures(Ri,Dc,Rhat, nLPP2b,nLPP3b, yDR,yDc,yD1)
    t1 = time.time()
    print('time getdFeatures',t1-t0)
    t0 = t1

    for i in range(10):
        _dyR, _dyDcos, _dyRn, _dyDn = \
                getDFeatures_yf(Ri,Dc,Rhat, nLPP2b,nLPP3b)
    t1 = time.time()
    print('time getdFeatures_yf',t1-t0)
    t0 = t1

    print("d_ml-yf dyR", cp.sum(cp.abs(dyR-_dyR)),cp.max(dyR-_dyR))
    print("d_ml-yf dyDcos", cp.sum(cp.abs(dyDcos-_dyDcos)),cp.max(dyDcos-_dyDcos))
    print("d_ml-yf dyRn", cp.sum(cp.abs(dyRn-_dyRn)),cp.max(dyRn-_dyRn))
    print("d_ml-yf dyDn", cp.sum(cp.abs(dyDn-_dyDn)),cp.max(dyDn-_dyDn))
    '''

    if pm.cupyFeat and (not pm.cupyMD):
        feat = cp.asnumpy(feat)
        dXidRl = cp.asnumpy(dXidRl)
        dXindRl= cp.asnumpy(dXindRl)
    return feat, dXidRl, dXindRl
   
#================================================================================
# test 
def test_getCos():
    """ getCos, getdCos
    """
    nLPP2b = 4
    Rc=6.2
    R0=1.8
    RcA = 2/(Rc-R0)
    RcB = (Rc+R0)/(Rc-R0)
    title= "Coslog_Rc"+str(Rc)+"_R0_"+str(R0)+"A_nFeat"+str(nLPP2b)
    
    x = cp.linspace(-2.0, 2.0, 500001)
    t0 = time.time()
    for i in range(100):
        y = getCos(x, nLPP2b)
    t1 = time.time()
    print('time getCos',t1-t0)
    t0 = t1

    for i in range(100):
        dy = getdCos(x, nLPP2b)
    t1 = time.time()
    print('time getdCos',t1-t0)
    t0 = t1

    x = cp.asnumpy(x)
    y = cp.asnumpy(y)
    dy = cp.asnumpy(dy)

    ax1=plt.subplot(211)
    plt.plot(x,y)
    plt.ylabel("getCos")
    plt.xlim((-2, 2))
    plt.title(title)
    plt.grid(True)
    plt.minorticks_on()
    
    ax2=plt.subplot(212)
    plt.plot(x,dy)
    plt.ylabel("getdCos")
    plt.xlabel("r(Angstrom)")
    plt.xlim((-2, 2))
    plt.grid(True)
    plt.minorticks_on()
    
    plt.tight_layout()
    plt.savefig(title+".png", dpi=300)
    plt.show()

    return

#test_getCos()
