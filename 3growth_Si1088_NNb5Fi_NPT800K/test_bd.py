#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from parameters import cupyMD
if cupyMD:
    import cupy as cp
else:
    import numpy as cp

import numpy as np
import ase.units as units
#from ase import Atoms
import parameters as pm
import sys

def scale_positions_and_cell_xyz_PWmat7(dt, taup, stressEnd, stressNow, n_scl_cell=10, i_xyz=[0, 1, 2], cell=1):
    """ NOTE: Try to be same as PWmat_md_7.
        Do the Berendsen pressure coupling,
        This dynamics scale the velocities and volumes to maintain a constant
        pressure and temperature.  The size of the unit cell is allowed to change
        independently in the three directions, but the angles remain constant.
    """

    taupscl = dt/ taup * n_scl_cell # MD_brendsen_cell_steps=10 in PWmat IN.MDOPT

    #stressNow = - stressNow * 1e-5 / units.Pascal
    stressNow = - stressNow 
    if stressNow.shape == (6,):
        stress = stressNow[:3]
    elif stressNow.shape == (3, 3):
        stress = [stressNow[i][i] for i in range(3)]
    else:
        stress = [stressNow for i in range(3)]

    scl_pressure = cp.ones((3,))
    #cell         = self.cell.copy()
    for i in i_xyz:
        scl_pressure[i] -= taupscl *(stressEnd-stress[i])
        if scl_pressure[i] > 1.01:
            scl_pressure[i] = 1.01
        if scl_pressure[i] < 0.99:
            scl_pressure[i] = 0.99
        cell = scl_pressure[i] * cell
        print("old_pressure, V_scaling, latt", i, stress[i], scl_pressure[i], cell, taupscl)
    return cell

#================================================================
#test 
f_data = sys.argv[1]
c, p = np.loadtxt(f_data, usecols=(2, 7), unpack=True)
n = c.shape
print("data.shape", n)
# ===================================
for i in range(10):
    V = 0.1583300000E+02 * 0.1152030000E+02 * c[i]
    stressNow = p[i] / V
    c1 = scale_positions_and_cell_xyz_PWmat7(pm.dt, pm.taup, pm.stressEnd, stressNow, pm.n_scl_cell, i_xyz=[2], cell=c[i])
    print(V, c[i], c1, c[i+1], (1-p[i]/V*2*10/120)*c[i], p[i], (1-c[i+1]/c[i])*120/2/10 *V, (1-c1/c[i])*120/2/10 *V )
