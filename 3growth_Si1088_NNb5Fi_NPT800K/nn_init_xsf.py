#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 20 12:15:58 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

import parameters as pm
if pm.cupyMD:
    import cupy as cp
    from nn_model_cupy import NNapiBase #,EiNN_cupy
else:  # use tf version
    import numpy as cp
    import tensorflow as tf
    from nn_model import NNapiBase #,EiNN

import numpy as np
import pandas as pd
import sys
import time

from func_data_scaler import mae, mse, mse4, get_feat_scl_nbxyz, get_feat_dfeat_scl_nbxyz
from func_struct import Atoms_cupy, get_neighbor_struct_cp

from ase import Atoms
from ase.io import read, write
from ase.units import fs, Bohr, eV, Ang, kB, Hartree
#print('fs, Bohr, eV, Ang, kB\n', fs, Bohr, eV, Ang, kB)
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution

class NNMD(NNapiBase):
    """
    NNMD

    parameters:

    """

    #===========================================================================
    def __init__(self, nn, data_scaler):

        super(NNMD, self).__init__( nn=nn, data_scaler=data_scaler)
        
        # below vars setted in self.init_structure()
        self.atoms_ase = None

    #===========================================================================
    def init_structure(self, f_atoms, f_initial, b_restart=False, f_restart=None, b_newTemp0=pm.b_newTemp0):
        # load ini structure
        # first get numpy arrays in CPU, 
        # then transfer to GPU cupy arrays
        self.atoms_ase = read(f_atoms)

        self.natoms = int(len(self.atoms_ase)/2)
        print(self.natoms, self.atoms_ase.get_cell())

        if b_restart == False:
            print("Will do NN_MD start as structure ", f_initial)
            df_PosForsV = pd.read_csv(f_initial, \
                    header=None,index_col=False,dtype="float", delim_whitespace=True)
            df_Ei = df_PosForsV.iloc[self.natoms*3:self.natoms*4, 1].values
            
            df_Pos = df_PosForsV.iloc[0:self.natoms, 1:4].values
            df_Pos2 = np.zeros([self.natoms*2, 3])
            df_Pos2[:self.natoms] = df_Pos
            df_Pos2[:self.natoms, 1] /= 2
            df_Pos2[self.natoms:] = df_Pos
            df_Pos2[self.natoms:, 1] /= 2
            # y += 0.5
            df_Pos2[self.natoms:, 1] += 0.5
            self.atoms_ase.set_scaled_positions(df_Pos2)
            
            df_Fi = df_PosForsV.iloc[self.natoms*1:self.natoms*2, 1:4].values
            df_Vi = df_PosForsV.iloc[self.natoms*2:self.natoms*3, 1:4].values
            #self.atoms_ase.set_velocities(df_Vi*Bohr/fs)
        else:
            print("Will restart NN_MD as the last structure in ", f_restart)
            df_PosForsV = pd.read_csv(f_restart, \
                    header=None,index_col=False,dtype="float", delim_whitespace=True)
            df_Ei  = df_PosForsV.iloc[0:self.natoms, 1].values
            df_Pos = df_PosForsV.iloc[0:self.natoms, 2:5].values
            df_Pos2 = np.zeros([self.natoms*2, 3])
            df_Pos2[:self.natoms] = df_Pos
            df_Pos2[self.natoms:] = df_Pos
            # y += 0.5
            df_Pos2[self.natoms:, 1] += self.atoms_ase.get_cell()[1,1]/2
            self.atoms_ase.set_positions(df_Pos2)
            
            df_Fi = -df_PosForsV.iloc[0:self.natoms, 5:8].values
            df_Vi = df_PosForsV.iloc[0:self.natoms, 8:11].values
            #self.atoms_ase.set_velocities(df_Vi)

        #NOTE: just for compare
        if b_newTemp0:
            print('init_structure() will use newTemp0 =', pm.temp0)
            MaxwellBoltzmannDistribution(self.atoms_ase, pm.temp0*kB, force_temp=True)  
        
        self.atoms_ase.write('Si204_111step765Yx21.xsf')
        sys.stdout.flush()

        return 

    #===========================================================================
