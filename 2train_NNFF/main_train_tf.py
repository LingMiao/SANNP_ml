#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Modified on Thu Aug 23 23:37:47 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

import numpy as np
import tensorflow as tf
import os

import parameters as pm
from func_data_scaler import DataScaler
from nn_model import EiNN
from nn_train import Trainer
from nn_md    import NNMD
from func_io import r_feat_csv

os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
os.environ["CUDA_VISIBLE_DEVICES"] = pm.cuda_dev
os.system('cat parameters.py')
#=======================================================================
with tf.device('/device:GPU:3'):
    nn = EiNN()
    data_scaler = DataScaler(f_ds=pm.f_data_scaler, f_feat=pm.f_train_feat)

    trainer = Trainer(nn, data_scaler)
    #sess = trainer.init_sess(pm.f_Einn_model)
    #nn.loadWij_np_check(sess, pm.f_Wij_np)

    itypes,feat,engy = r_feat_csv(pm.f_train_feat)
    sess = trainer.init_sess(pm.f_Einn_model)
    
    trainer.train_Ei(pm.f_pretr_feat, pm.f_test_feat, pm.epochs_pretrain, nn_file=pm.d_nnEi+'preEi', eMAE_err=pm.eMAE_err)
    trainer.train_Ei(pm.f_train_feat, pm.f_test_feat, pm.epochs_alltrain, nn_file=pm.d_nnEi+'allEi', eMAE_err=pm.eMAE_err)
    nn.saveWij_np(sess, pm.f_Wij_np+'_0Ei')
    trainer.getEi_err(itypes,feat,engy, outfile=pm.f_train_feat+'_0Ei_ckEi')
    trainer.getFi_err(pm.f_train_nblt, outfile=pm.f_train_feat+'_0Ei_ckFi')

    # sess = trainer.init_sess(pm.f_Einn_model)
    # nn.loadWij_np_check(sess, pm.f_Wij_np+'_0Ei.npy')
    # trainer.train_Ei('./Si204_zbk201902_PWmat/MVMTnpt_someatoms3_feat_test.csv', \
    #         pm.f_test_feat, 300, nn_file=pm.d_nnEi+'0Ei1bEi', eMAE_err=pm.eMAE_err)
    # nn.saveWij_np(sess, pm.f_Wij_np+'_0Ei1bEi')
    # trainer.getEi_err(itypes,feat,engy, outfile=pm.f_train_feat+'_0Ei1bEi_ckEi')
    # trainer.getFi_err(pm.f_train_nblt, outfile=pm.f_train_feat+'_0Ei1bEi_ckFi')

    # trainer.train_Ei(pm.f_train_feat, \
    #         pm.f_test_feat, 100, nn_file=pm.d_nnEi+'0Ei1bEi0Ei', eMAE_err=pm.eMAE_err)
    # nn.saveWij_np(sess, pm.f_Wij_np+'_0Ei1bEi0Ei')
    # trainer.getEi_err(itypes,feat,engy, outfile=pm.f_train_feat+'_0Ei1bEi0Ei_ckEi')
    # trainer.getFi_err(pm.f_train_nblt, outfile=pm.f_train_feat+'_0Ei1bEi0Ei_ckFi')

    # trainer.train_Fi(pm.f_train_nblt, pm.f_test_nblt, pm.epochs_Fi_train, nn_file=pm.d_nnFi+'Fi', iFi_repeat=pm.iFi_repeat)
    # nn.saveWij_np(sess, pm.f_Wij_np)
    # trainer.getEi_err(itypes,feat,engy, outfile=pm.f_train_feat+'_0Fi_ckEi')
    # trainer.getFi_err(pm.f_train_nblt, outfile=pm.f_train_feat+'_0Fi_ckFi')

    # sess = trainer.init_sess(pm.f_Einn_model)
    # nn.loadWij_np_check(sess, pm.f_Wij_np)
    # nn.saveWij_np(sess, pm.f_Wij_np+'_0Fi')

    # trainer.train_Ei(pm.f_train_feat, \
    #         pm.f_test_feat, 11, isaveNN=10, nn_file=pm.d_nnEi+'0Ei1bEi0Ei0Fi0Ei', eMAE_err=pm.eMAE_err)
    # nn.saveWij_np(sess, pm.f_Wij_np+'_0Ei1bEi0Ei0Fi0Ei')
    # trainer.getEi_err(itypes,feat,engy, outfile=pm.f_train_feat+'_0Ei1bEi0Ei0Fi0Ei_ckEi')
    # trainer.getFi_err(pm.f_train_nblt, outfile=pm.f_train_feat+'_0Ei1bEi0Ei0Fi0Ei_ckFi')

    # sess = trainer.init_sess(pm.f_Einn_model)
    # nn.loadWij_np_check(sess, pm.f_Wij_np+'_0Ei1bEi0Ei.npy')

    # trainer.train_Ei(pm.f_train_feat, \
    #         pm.f_test_feat, 501, nn_file=pm.d_nnEi+'0Ei1bEi0Ei2biasEi', eMAE_err=pm.eMAE_err)
    # nn.saveWij_np(sess, pm.f_Wij_np+'_0Ei1bEi0Ei2biasEi')
    # trainer.getEi_err(itypes,feat,engy, outfile=pm.f_train_feat+'_0Ei1bEi0Ei2biasEi_ckEi')
    # trainer.getFi_err(pm.f_train_nblt, outfile=pm.f_train_feat+'_0Ei1bEi0Ei2biasEi_ckFi')

    # trainer.train_Fi(pm.f_train_nblt, pm.f_test_nblt, pm.epochs_Fi_train, nn_file=pm.d_nnFi+'Fi', iFi_repeat=pm.iFi_repeat)
    # nn.saveWij_np(sess, pm.f_Wij_np+'_0Ei1bEi0Ei2biasEi2Fi')
    # trainer.getEi_err(itypes,feat,engy, outfile=pm.f_train_feat+'_2Fi_ckEi')
    # trainer.getFi_err(pm.f_train_nblt, outfile=pm.f_train_feat+'_2Fi_ckFi')

    # sess = trainer.init_sess(pm.f_Einn_model)
    # nn.loadWij_np_check(sess, pm.f_Wij_np+'4EiFi.npy')

    # trainer.train_Ei(pm.f_train_feat, \
    #         pm.f_test_feat, 501, isaveNN=10, nn_file=pm.d_nnEi+'5biasEi', eMAE_err=pm.eMAE_err)
    # nn.saveWij_np(sess, pm.f_Wij_np+'5biasEi')
    # trainer.getEi_err(itypes,feat,engy, outfile=pm.f_train_feat+'5biasEi_ckEi')
    # trainer.getFi_err(pm.f_train_nblt, outfile=pm.f_train_feat+'5biasEi_ckFi')

    # trainer.train_Fi(pm.f_train_nblt, pm.f_test_nblt, pm.epochs_Fi_train, nn_file=pm.d_nnFi+'5EiFi', iFi_repeat=pm.iFi_repeat)
    # nn.saveWij_np(sess, pm.f_Wij_np+'5EiFi')
    # trainer.getEi_err(itypes,feat,engy, outfile=pm.f_train_feat+'5EiFi_ckEi')
    # trainer.getFi_err(pm.f_train_nblt, outfile=pm.f_train_feat+'5EiFi_ckFi')

    # trainer.train_Ei(pm.f_train_feat, \
    #         pm.f_test_feat, 11, isaveNN=10, nn_file=pm.d_nnEi+'5EiFiEi', eMAE_err=pm.eMAE_err)
    # nn.saveWij_np(sess, pm.f_Wij_np+'5EiFiEi')
    # trainer.getEi_err(itypes,feat,engy, outfile=pm.f_train_feat+'5EiFiEi_ckEi')
    # trainer.getFi_err(pm.f_train_nblt, outfile=pm.f_train_feat+'5EiFiEi_ckFi')
