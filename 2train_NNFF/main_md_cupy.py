#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Modified on Thu Aug 23 23:37:47 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

import parameters as pm
from func_data_scaler import DataScaler
from nn_model_cupy import EiNN_cupy
from nn_md         import NNMD

import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
os.environ["CUDA_VISIBLE_DEVICES"] = pm.cuda_dev
os.system('cat parameters.py')
#=======================================================================
nn = EiNN_cupy(f_Wij_np=pm.f_Wij_np)
data_scaler = DataScaler(f_ds=pm.f_data_scaler, f_feat=pm.f_pretr_feat)

nnmd = NNMD(nn, data_scaler)
nnmd.NPT_berendsen(pm.f_atoms, pm.f_initial)


