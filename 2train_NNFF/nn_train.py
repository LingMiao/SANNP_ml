#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Modified on Thu Aug 23 23:37:47 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

import numpy as np
import pandas as pd
import tensorflow as tf
import sys
import time

import parameters as pm
from func_io import r_feat_csv
from func_data_scaler import mae, mse, maeEtot, delta, get_feat_dfeat_scl_df
from nn_model import EiNN, NNapiBase

class Trainer(NNapiBase):
    """
    Trainer

    parameters:

    """

    #===========================================================================
    def __init__(self, nn, data_scaler, \
            learning_rate=pm.learning_rate, rtLossE=pm.rtLossE, rtLossF=pm.rtLossF ):

        super(Trainer, self).__init__(nn=nn, data_scaler=data_scaler)

        self.rtLossE = rtLossE
        self.rtLossF = rtLossF

        # === self.lossEi and optimizer  ===
        self.lossEi = tf.reduce_mean(tf.squared_difference(self.pred_Ei, (1.10)*self.YEi)) 
        self.lossFi = tf.reduce_mean(tf.squared_difference(self.pred_Fi, self.YFi))
        self.lossEF = rtLossE *self.lossEi + rtLossF *self.lossFi
        
        self.optEi  = tf.train.AdamOptimizer(learning_rate).minimize(self.lossEi)
        self.optFi  = tf.train.AdamOptimizer(learning_rate).minimize(self.lossEF)

    #===========================================================================
    def train_Ei(self, f_train, f_test, epochs,\
            iprint=10, isaveNN=100, nn_file=pm.d_nnEi+'pre', eMAE_err=pm.eMAE_err):
        """
        """
        print("\n=train_Ei start ", f_train, epochs, "epochs, at ", time.ctime())
        itypes,feat,engy = r_feat_csv(f_train)
        feat_scaled = self.ds.pre_feat(feat)
        engy_scaled = self.ds.pre_Ei(engy)
        nImg = int(feat_scaled.shape[0]/self.natoms)
        #itypes = np.reshape(np.expand_dims(self.itp_uc, 0) *np.ones((nImg, self.natoms), dtype=int), [-1])

        print("  and test file ", f_test)
        itypes_t,feat_t,engy_t = r_feat_csv(f_test)

        for epoch in range(epochs):
            self.sess.run(self.optEi, \
                    feed_dict={self.X:feat_scaled, self.YEi:engy_scaled, self.itp:itypes})
        
            if epoch % iprint == 0:
                training_cost, engy_out = self.sess.run(\
                        (self.lossEi, self.pred_Ei), \
                        feed_dict={self.X:feat_scaled, self.YEi:engy_scaled, self.itp:itypes})
                Ep = self.ds.post_Ei(engy_out)
                print(epoch, training_cost, mae(Ep,engy), mse(Ep,engy), maeEtot(Ep,engy,self.natoms), time.ctime())
                sys.stdout.flush()
            if epoch % isaveNN == 0:
                save_path = self.saver.save(self.sess, nn_file+str(epoch)+".ckpt")
                print("Model saved: {}".format(save_path))

                test_cost, test_MAE = self.getEi_err(itypes_t,feat_t,engy_t)
                train_MAE = mae(Ep,engy)
                if ( test_MAE > train_MAE*1.05) \
                        &( train_MAE < eMAE_err ):
                    print("=== convergence with test data at ", time.ctime())
                    break
        
                # for debug delta
                delta(Ep,engy)

        save_path = self.saver.save(self.sess, nn_file+"_final.ckpt", write_state=False)
        print("Model saved: {}".format(save_path))
        print("=train_Ei end === at ", time.ctime())
        sys.stdout.flush()
    
    #===========================================================================
    def getEi_err(self, itypes,feat,engy, outfile=None, b_print=True):
        """
        itypes,feat,engy = r_feat_csv(f_test)
        """
        feat_scaled = self.ds.pre_feat(feat)
        engy_scaled = self.ds.pre_Ei(engy)
        nImg = int(feat_scaled.shape[0]/self.natoms)
        #itypes = np.reshape(np.expand_dims(self.itp_uc, 0) *np.ones((nImg, self.natoms), dtype=int), [-1])
        
        test_cost, test_engy = self.sess.run(\
                (self.lossEi, self.pred_Ei), \
                feed_dict={self.X: feat_scaled, self.YEi: engy_scaled, self.itp:itypes})
        Ep = self.ds.post_Ei(test_engy)

        if outfile != None :
            out_Ep = np.concatenate([engy, Ep, Ep -engy], axis=1)
            df_out = pd.DataFrame(out_Ep)
            df_out.to_csv(outfile, mode='a', header=False)
        
        if b_print == True :
            #print("=getEi_err "+f_test+" =")
            print("=getEi_err_test =")
            print("MAE, MSE, MAE_Etot: ", mae(Ep,engy), mse(Ep,engy), maeEtot(Ep,engy,self.natoms))
            sys.stdout.flush()
    
        return test_cost, mae(Ep,engy)
    
    #===========================================================================
    def train_Fi(self, f_train_nblt, f_test_nblt, epochs, \
            nn_file=pm.d_nnFi+'Fi', iFi_repeat=20):

        print("\n=train_Fi start ", f_train_nblt, epochs, "epochs, at ", time.ctime())
        for epoch in range(epochs):
            print("=== ", epoch,"th forces training begin at ", time.ctime())
            df_orig_chunk = pd.read_csv(f_train_nblt, header=None,index_col=False,dtype="float", \
                    iterator=True,chunksize=self.natoms)
    
            img = 0    
            for df_orig in df_orig_chunk:
            #for i in range(10):  # only for debug
            #    df_orig = df_orig_chunk.get_chunk()

                img += 1    
                engy_scaled, fors_scaled, feat_scaled, feat_d1, feat_d2, idxNb, engy, fors = \
                        get_feat_dfeat_scl_df(df_orig, self.ds)
                # === Key point!!!
                for i in range(iFi_repeat):
                    lossEF_, engy_out, f_out, loss_, lossF_ = \
                            self.sess.run( \
                            (self.optFi, \
                            self.pred_Ei, self.pred_Fi, self.lossEi, self.lossFi), \
                            feed_dict={self.X:feat_scaled, \
                            self.tfdXi:feat_d1, self.tfdXin:feat_d2, self.tf_idxNb:idxNb,\
                            self.YEi:engy_scaled, self.YFi:fors_scaled, self.itp:self.itp_uc})
    
                    Ep = self.ds.post_Ei(engy_out)
                    Fp = self.ds.post_Fi(f_out)
                    print(img, "loss_F: ", lossF_, \
                            "\tloss_E: ", loss_ , \
                            " losstot:", self.rtLossE *loss_ + self.rtLossF *lossF_, \
                            "at ", time.ctime())
                    print(img, "Fi_MSE: ", mse(Fp,fors), \
                            "\tEi_MSE: ", mse(Ep,engy), \
                            " dEi_sum", np.sum(Ep-engy) )
                    sys.stdout.flush()
        
            print("=== ", epoch, "th check error at ", time.ctime())
            #self.getEi_err(pm.f_test_feat)
            self.getFi_err(f_test_nblt)
              
            save_path = self.saver.save(self.sess, nn_file+str(epoch)+".ckpt")
            print("Model saved: {}".format(save_path))
            sys.stdout.flush()
          
        save_path = self.saver.save(self.sess, nn_file+"_final.ckpt", write_state=False)
        print("Model saved: {}".format(save_path))
        print("=train_Fi end === at ", time.ctime())
        sys.stdout.flush()

    #=======================================================================
    def getFi_err(self, f_test_nblt, outfile=None, b_print=True):

        df_orig_chunk = pd.read_csv(f_test_nblt, header=None,index_col=False,dtype="float", \
                iterator=True,chunksize=self.natoms)
        
        FiMAE=0.
        Eisum_MAE=0.
        FxyzMSE = 0.
        EiMSE = 0.
        n = 0
        
        for df_orig in df_orig_chunk:
        #for i in range(10):  # only for debug
        #    df_orig = df_orig_chunk.get_chunk()

            engy_scaled, fors_scaled, feat_scaled, feat_d1, feat_d2, idxNb, engy, fors = \
                    get_feat_dfeat_scl_df(df_orig, self.ds)
            engy_out, f_out = \
                    self.sess.run( \
                    (self.pred_Ei, self.pred_Fi), \
                    feed_dict={self.X:feat_scaled, \
                    self.tfdXi:feat_d1, self.tfdXin:feat_d2, self.tf_idxNb:idxNb,\
                    self.YEi:engy_scaled, self.YFi:fors_scaled, self.itp:self.itp_uc})

            Ep = self.ds.post_Ei(engy_out)
            Fp = self.ds.post_Fi(f_out)
    
            FiMAE0 = np.sum(np.abs( \
                    np.sqrt(np.sum(Fp**2,axis=1))-np.sqrt(np.sum(fors**2,axis=1))\
                    ))/self.natoms
            FiMAE += FiMAE0
            Eisum_MAE0 = np.abs(np.sum(Ep-engy))/self.natoms
            Eisum_MAE += Eisum_MAE0
            
            FxyzMSE0 = mse(Fp,fors)
            FxyzMSE += FxyzMSE0
            EiMSE0 = mse(Ep,engy)
            EiMSE += EiMSE0
            n += 1
    
            #morning 20180613
            if outfile != None :
                out_Fp = np.concatenate([engy, Ep, engy -Ep, fors, Fp, fors -Fp], axis=1)
                df_out = pd.DataFrame(out_Fp)
                df_out.to_csv(outfile, mode='a', header=False)
    
            if b_print == True :
                print(n, "th data check ===")
                print("Esum_MAE0: ", Eisum_MAE0, "\tEiMSE0: ", EiMSE0)
                print("|Fi|_MAE0: ", FiMAE0,     "\tFxyzMSE0:", FxyzMSE0)
                sys.stdout.flush()
        
        print("=getFi_err " +f_test_nblt+ " = ")
        print("Esum_MAE: ", Eisum_MAE/n, "\tEiMSE: ", EiMSE/n)
        print("|Fi|_MAE: ", FiMAE/n,     "\tFxyzMSE:", FxyzMSE/n)
        sys.stdout.flush()
    #=======================================================================
