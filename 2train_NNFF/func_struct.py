#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 21 16:00:24 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

from parameters import cupyMD
if cupyMD:
    import cupy as cp
else:
    import numpy as cp

import numpy as np
import ase.units as units
#from ase import Atoms
import parameters as pm

#================================================================================
class Atoms_cupy:
    """Atoms object, very simple cupy version, only for NNMD calculation at GPU
    """
    def __init__(self):
        self.natoms     = 0   # 
        self.cell       = 0   # 3x3, Ang
        self.numbers    = 0   # nx1
        self.masses     = 0   # nx1, atomic unit
        self.positions  = 0   # nx3, Ang
        self.forces     = 0   # nx3, Ang/eV
        self.velocities = 0   # nx3, ?
        self.fixmask    = 1   # nx3, fix mask, 1 move, 0 fix
        return
        
    def set_from_aseAtoms(self, atoms_ase, fixmask=None):
        self.cell       = cp.asarray(atoms_ase.get_cell(complete=True)     )
        self.numbers    = cp.asarray(atoms_ase.get_atomic_numbers()        )
        self.masses     = cp.asarray(atoms_ase.get_masses()[:, cp.newaxis] )
        self.positions  = cp.asarray(atoms_ase.get_positions(wrap=False)   )
        self.velocities = cp.asarray(atoms_ase.get_velocities()            )
        #fixmask         = cp.asarray(fixmask                               )
        #self.forces     = 
        self.natoms     = self.positions.shape[0]

        if cp.size(fixmask) != (self.natoms,3):
            print("Warning, fixmask.size is not (natoms,3), will all move.")
            self.fixmask= cp.ones((self.natoms,3))
        else:
            self.fixmask= cp.asarray(fixmask)
            self.velocities = self.velocities * self.fixmask

        return

    def set_to_aseAtoms(self, atoms_ase):
        print("Will implement soon ^_^!")
        return

    def get_positions(self, wrap=False):
        """Get array of positions. If wrap==True, wraps atoms back
        into unit cell.
        """
        if wrap:
            scaled = self.get_scaled_positions()
            return cp.dot(scaled, self.cell)
        else:
            return self.positions.copy()

    def get_scaled_positions(self, wrap=True):
        """Get positions relative to unit cell."""
        # fractional = cp.linalg.solve(self.cell.T,
        #                              self.positions.T).T

        # NOTE: onle for 90 orthorhombic lattice
        fractional = cp.zeros_like(self.positions)
        fractional[:,0] = self.positions[:,0]/self.cell[0,0]
        fractional[:,1] = self.positions[:,1]/self.cell[1,1]
        fractional[:,2] = self.positions[:,2]/self.cell[2,2]

        if wrap:
            # Yes, we need to do it twice.
            fractional %= 1.0
            fractional %= 1.0
        return fractional

    def set_scaled_positions(self, scaled):
        """Set positions relative to unit cell."""
        self.positions[:] = cp.dot(scaled, self.cell)
        return

    def get_kinetic_energy(self):
        return 0.5 * cp.vdot(self.velocities*self.masses, self.velocities)

    def get_temperature(self):
        return 2*self.get_kinetic_energy()/(self.natoms*3 *units.kB)
        
    def get_volume(self):
        return abs(cp.linalg.det(self.cell))

    def set_cell(self, cell, scale_atoms=False):
        """Set unit cell vectors."""
        cell = cp.array(cell, float)

        if cell.shape == (3,):
            cell = cp.diag(cell)
        elif cell.shape != (3, 3):
            raise ValueError('Cell must be length 3 sequence, length 6 '
                             'sequence or 3x3 matrix!')

        if scale_atoms:
            pos_cry = self.get_scaled_positions(wrap=True)
            self.positions[:] = cp.dot(pos_cry, cell)
        self.cell = cell
        return

    def scale_velocities(self, dt, taut, tempEnd):
        """ Do the NVT Berendsen velocity scaling """
        tautscl = dt / taut
        old_temperature = self.get_temperature()

        scl_temperature = cp.sqrt(1.0 +
                                  (tempEnd / old_temperature - 1.0) *
                                  tautscl)
        # Limit the velocity scaling to reasonable values
        if scl_temperature > 1.1:
            scl_temperature = 1.1
        if scl_temperature < 0.9:
            scl_temperature = 0.9
        
        self.velocities *= scl_temperature
        return

    def scale_positions_and_cell(self, dt, taup, stressEnd, stressNow):
        """ Do the Berendsen pressure coupling,
        scale the atom position and the simulation cell."""

        taupscl = dt / taup
        #old_pressure = -stressNow.trace() / 3 * 1e-5 / units.Pascal
        old_pressure = -stressNow.trace() / 3
        scl_pressure = (1.0 - taupscl / 3.0 *
                        (stressEnd - old_pressure))

        #print("taupscl, units.Pascal", taupscl, units.Pascal)
        print("old_pressure, V_scaling", old_pressure, scl_pressure)
        if scl_pressure > 1.01:
            scl_pressure = 1.01
        if scl_pressure < 0.99:
            scl_pressure = 0.99

        cell = self.cell.copy()
        self.set_cell(cell*scl_pressure, scale_atoms=True)
        return

    def scale_positions_and_cell_xyz(self, dt, taup, stressEnd, stressNow, i_xyz=[0, 1, 2]):
        """ Do the Berendsen pressure coupling,
            This dynamics scale the velocities and volumes to maintain a constant
            pressure and temperature.  The size of the unit cell is allowed to change
            independently in the three directions, but the angles remain constant.
        """

        taupscl = dt / taup / 3.0
        #stressNow = - stressNow * 1e-5 / units.Pascal
        stressNow = - stressNow 
        if stressNow.shape == (6,):
            stress = stressNow[:3]
        elif stressNow.shape == (3, 3):
            stress = [stressNow[i][i] for i in range(3)]
        else:
            raise ValueError('Cannot use a stress tensor of shape ' +
                             str(stress.shape))

        scl_pressure = cp.ones((3,))
        cell         = self.cell.copy()
        for i in i_xyz:
            scl_pressure[i] -= taupscl *(stressEnd-stress[i])
            if scl_pressure[i] > 1.01:
                scl_pressure[i] = 1.01
            if scl_pressure[i] < 0.99:
                scl_pressure[i] = 0.99
            cell[i,i] = scl_pressure[i] * self.cell[i,i]
            print("old_pressure, V_scaling", i, stress[i], scl_pressure[i])

        self.set_cell(cell, scale_atoms=True)
        return

    #================================================================================
    def scale_positions_and_cell_xyz_PWmat7(self, dt, taup, stressEnd, stressNow, n_scl_cell=10, i_xyz=[0, 1, 2]):
        """ NOTE: Try to be same as PWmat_md_7.
            Do the Berendsen pressure coupling,
            This dynamics scale the velocities and volumes to maintain a constant
            pressure and temperature.  The size of the unit cell is allowed to change
            independently in the three directions, but the angles remain constant.
        """

        taupscl = dt/ taup * n_scl_cell # MD_brendsen_cell_steps=10 in PWmat IN.MDOPT
        #stressNow = - stressNow * 1e-5 / units.Pascal
        stressNow = - stressNow 
        if stressNow.shape == (6,):
            stress = stressNow[:3]
        elif stressNow.shape == (3, 3):
            stress = [stressNow[i][i] for i in range(3)]
        else:
            raise ValueError('Cannot use a stress tensor of shape ' +
                             str(stress.shape))

        scl_pressure = cp.ones((3,))
        cell         = self.cell.copy()
        for i in i_xyz:
            scl_pressure[i] -= taupscl *(stressEnd-stress[i])
            if scl_pressure[i] > 1.01:
                scl_pressure[i] = 1.01
            if scl_pressure[i] < 0.99:
                scl_pressure[i] = 0.99
            cell[i,i] = scl_pressure[i] * self.cell[i,i]
            print("old_pressure, V_scaling, latt", i, stress[i], scl_pressure[i], cell[i,i])

        self.set_cell(cell, scale_atoms=True)
        return

#================================================================================
def get_neighbor_struct_cp(cell, pos, R0=pm.R0, Rc=pm.Rc, maxNb=pm.maxNb):
    '''Analysis atomic structure to get local neighbor structure for each atom.
         If num_nb_atoms > maxNb in Rc, neighbors_list are sorted with dij, 
         and will remain nearest maxNb atoms, and may generate un-exact feature! 
       parameters:
         cell: 3x3 cell lattice, Ang
         pos: nx3 atomic postion, Ang
       return:
         nNb:  nx1, number of neighbor atoms in Rc
         idxNb: nxm, index (in cell) of neighbor atoms in Rc
         nbxyz: nxmx3, relative postion of neighbor atoms i
    '''
    natoms = pos.shape[0]

    # like in ase.atoms class
    # NOTE!!! need debug for cupy version
    #Rcry = cp.linalg.solve(cell.T, pos.T).T
    Rcry = cp.zeros_like(pos)
    Rcry[:,0] = pos[:,0]/cell[0,0]
    Rcry[:,1] = pos[:,1]/cell[1,1]
    Rcry[:,2] = pos[:,2]/cell[2,2]

    Rcry %= 1.0
    Rcry %= 1.0
    #_Rcry = Rcry.copy()
    #_Rcry[Rcry >1] = Rcry[Rcry >1] - cp.floor(Rcry[Rcry >1])
    #_Rcry[Rcry <0] = Rcry[Rcry <0] - cp.floor(Rcry[Rcry <0])
    #print('d_Rcry', cp.sum(cp.abs(Rcry -_Rcry)))

    Rd = Rcry[None,:,:] - Rcry[:,None,:]
    Rd[Rd> 0.5] = Rd[Rd> 0.5] - 1
    Rd[Rd<-0.5] = Rd[Rd<-0.5] + 1
    # to wraped in cell with Angstrom
    Rd = cp.matmul(Rd, cell.T)   
    
    Rd2 = cp.sum(Rd**2,axis=2)
    Rd[Rd2>Rc**2] = 0
    # idxMask also exclude i-th atom-self, comparing with RcMask
    idxMask = cp.sum(Rd**2,axis=2)>0
    nNb = idxMask.sum(axis=1)

    if cp.min(Rd2[Rd2>0]) < R0**2:
        iR0 = cp.where((0<Rd2) * (Rd2<R0**2))  # True*True = Ture
        print('===Warning in get_neighbor_struct_cp():', \
                iR0, 'th atoms have ', Rd2[iR0], 'distanse < R0', R0, \
                " Will generate wrong feature!" )
    if cp.max(nNb) > maxNb:
        # to sort as d and make a cutoff of maxNb atoms
        isort = cp.where(nNb>maxNb)
        print('===Warning in get_neighbor_struct_cp():', \
                cp.where(nNb>maxNb)[0], 'th atoms have ', nNb[nNb>maxNb], 'neighbors > maxNb', maxNb, \
                " Will remain nearest maxNb atoms, and generate un-exact feature!" )
        for i in cp.where(nNb>maxNb)[0]:
            di = cp.sum(Rd[i]**2,axis=1)
            arg_id    = cp.argsort(di)
            Rd[i, arg_id[natoms-nNb[i]+maxNb:]] = 0
            #print("Rd[i] !=0 ", cp.sum(Rd[i, arg_id[natoms-nNb[i]-1:]]**2,axis=1))
        idxMask = cp.sum(Rd**2,axis=2)>0
        nNb = idxMask.sum(axis=1)

    iidx, jidx = cp.where(idxMask)
    jidx2 = cp.concatenate([cp.arange(0, int(nNb[i])) for i in range(natoms)]).astype(int)

    idxNb = cp.zeros((natoms,maxNb)).astype(int)
    idxNb[(iidx,jidx2)] = jidx+1
    
    nbxyz = cp.zeros((natoms,maxNb,3))
    nbxyz[(iidx,jidx2)] = Rd[idxMask]

    return nNb, idxNb, nbxyz

#================================================================================
def get_atoms_type(f_atoms):
    """read atoms.xsf file
       return itypes as atomic numbers of each atom
              at_types as atomic numbers and num of each type
              at_types_idx as atom index of each type
    """
    from ase.io import read

    atoms_str = read(f_atoms)
    natoms = len(atoms_str)
    itypes = atoms_str.get_atomic_numbers()

    import collections
    at_type_count = collections.Counter(itypes)
    at_types = np.array(list(at_type_count.items()))
    assert natoms == np.sum(at_types[:,1])
    
    at_types_idx = np.zeros((at_types.shape[0], natoms)).astype(int)
    for i in range(at_types.shape[0]):
        at_types_idx[i,:at_types[i,1]]=np.array(np.where(itypes==at_types[i,0]))

    print("=get_atoms_type()", f_atoms, '\n',
            "natoms", natoms, '\n',
            "at_types", at_types )
    return itypes, at_types, at_types_idx

#================================================================================
