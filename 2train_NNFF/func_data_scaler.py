#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Modified on Fri Aug 24 00:11:27 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

from parameters import cupyMD
if cupyMD:
    import cupy as cp
else:
    import numpy as cp

import numpy as np
import pandas as pd
#from sklearn.preprocessing import MinMaxScaler

#================================================================================
def mae(Enn, Edft):
    return cp.mean(cp.abs(Enn-Edft))

def mse(Enn, Edft):
    return cp.sqrt(cp.mean((Enn-Edft)**2))

def mse4(Enn, Edft, scl=10):
    return cp.mean((scl*(Enn-Edft))**4)

def maeEtot(Enn, Edft, natoms):
    dEtot = cp.sum((Enn-Edft).reshape((-1,natoms)), axis=1)
    return cp.mean(cp.abs(dEtot))

def delta(Enn, Edft):
    Edft_ = Edft -cp.mean(Edft, dtype='float64')
    Enn_  = Enn -cp.mean(Edft, dtype='float64')
    dE    = Enn -Edft
    deltaDFT = cp.sum(dE *Edft_) / cp.sum(Edft_**2).astype('float32')
    deltaNN  = cp.sum(dE *Enn_)  / cp.sum(Edft_**2).astype('float32')
    print('deltaDFT, deltaNN', deltaDFT, deltaNN)
    return deltaDFT

#================================================================================
class MinMaxScaler:
    ''' a*x +b = x_scaled like sklearn's MinMaxScaler
        note cp.atleast_2d and self.a[xmax==xmin] = 0
    '''
    def __init__(self, feature_range=(0,1)):
        self.fr = feature_range
        self.a = 0
        self.b = 0

    def fit_transform(self, x):
        x = cp.atleast_2d(x)

        xmax = x.max(axis=0)
        xmin = x.min(axis=0)

        self.a = (self.fr[1] -self.fr[0]) / (xmax-xmin).astype('float64')
        self.a[xmax==xmin] = 0  # important !!!
        #self.a[cp.isinf(self.a)] = 0  # important !!!
        self.b = self.fr[0] - self.a*xmin

        return self.transform(x)

    def transform(self, x):
        x = cp.atleast_2d(x)
        return (self.a*x +self.b).astype('float32')

    def inverse_transform(self, y):
        y = cp.atleast_2d(y)
        return ((y -self.b) /self.a).astype('float32')

#================================================================================
class AveMaxScaler:
    ''' a*x +b = x_scaled like sklearn's MinMaxScaler
        note cp.atleast_2d and self.a[xmax==xmin] = 0
    '''
    def __init__(self, feature_range=(0,1)):
        self.fr = feature_range
        self.a = 0
        self.b = 0

    def fit_transform(self, x):
        x = cp.atleast_2d(x)

        xmax = x.max(axis=0)
        xmin = x.min(axis=0)
        # NOTE dtype='float64'
        xave = cp.mean(x, axis=0, dtype='float64')

        self.a = (self.fr[1] -self.fr[0]) / (xmax-xave)
        self.a[xmax==xave] = 0  # important !!!
        #self.a[cp.isinf(self.a)] = 0  # important !!!
        self.b = self.fr[0] - self.a*xave

        print('debug: fit_transform(), self.a', self.a, self.a.dtype)
        print('debug: fit_transform(), self.b', self.b, self.b.dtype)
        print('debug: fit_transform(), xmax, xmin, xave', xmax, xmin, xave)
        print('debug: fit_transform(), x.shape', x.shape)
        print('debug: fit_transform(), x.sum', x.sum(axis=0))
        print('debug: fit_transform(), scled_x.sum', self.transform(x).sum(axis=0))

        return self.transform(x)

    def transform(self, x):
        x = cp.atleast_2d(x)
        return (self.a*x +self.b).astype('float32')

    def inverse_transform(self, y):
        y = cp.atleast_2d(y)
        return ((y -self.b) /self.a).astype('float32')

#================================================================================
class DataScaler:
    def __init__(self, f_ds, f_feat):
        # self.feat_scaler=MinMaxScaler(feature_range=(0,1))
        # self.engy_scaler=MinMaxScaler(feature_range=(0,1))
        self.feat_scaler=AveMaxScaler(feature_range=(0,1))
        self.engy_scaler=AveMaxScaler(feature_range=(0,1))
        self.feat_a = None
        self.engy_a = None

        from os import path
        if path.isfile(f_feat):
            self.f_scaler_from_data = f_feat
            self.get_scaler(f_feat, f_ds, b_save=True)
        elif path.isfile(f_ds):
            self.loadDS_np(f_ds)
        else:
            print("===Error in DataScaler, don't find ", f_ds, f_feat, '===')
            self.loadDS_np(f_ds)

        return


    def get_scaler(self, f_feat, f_ds, b_save=True):
        from func_io import r_feat_csv

        itypes,feat,engy = r_feat_csv(f_feat)
        print('=DS.get_scaler ', f_feat, 'feat.shape, feat.dtype', feat.shape, feat.dtype)
        print('    Ei.min and max are', cp.min(engy), cp.max(engy), 'eV')
        
        _ = self.feat_scaler.fit_transform(feat)
        _ = self.engy_scaler.fit_transform(engy)
        
        feat_b      = self.feat_scaler.transform(cp.zeros((1, feat.shape[1])))    
        self.feat_a = self.feat_scaler.transform(cp.ones((1, feat.shape[1]))) - feat_b
        engy_b      = self.engy_scaler.transform(0)
        self.engy_a = self.engy_scaler.transform(1) - engy_b
        #print('engy_b', engy_b.shape)
        #print('engy_scaler', self.engy_scaler.scale_.shape, self.engy_scaler.min_.shape)

        if b_save and (not cupyMD):
            self.save2np(f_ds)
    
        return self.feat_scaler, self.engy_scaler
    
    def pre_feat(self, feat):
        return self.feat_scaler.transform(feat)

    def pre_dfeat(self, dXidRl, dXindRl):
        return self.feat_a[:,:,cp.newaxis] *dXidRl, self.feat_a[:,:,cp.newaxis] *dXindRl
        
    def pre_Ei(self, engy):
        return self.engy_scaler.transform(engy)
    def post_Ei(self, engy_out):
        return self.engy_scaler.inverse_transform(engy_out)
    
    def pre_Fi(self, fors):
        #return fors* self.engy_a[0,0]
        return fors* self.engy_a[0]
    def post_Fi(self, fors_out):
        #return fors_out/ self.engy_a[0,0]
        return fors_out/ self.engy_a[0]
        
    def save2np(self, f_npfile):
        dsnp = []
        dsnp.append(np.array(self.feat_scaler.fr))
        dsnp.append(np.array(self.feat_scaler.a))
        dsnp.append(np.array(self.feat_scaler.b))
        dsnp.append(np.array(self.engy_scaler.fr))
        dsnp.append(np.array(self.engy_scaler.a))
        dsnp.append(np.array(self.engy_scaler.b))
        dsnp.append(np.array(self.feat_a))
        dsnp.append(np.array(self.engy_a))

        dsnp = np.array(dsnp)
        np.save(f_npfile, dsnp)
        print('DataScaler.save2np to', f_npfile, dsnp.dtype, dsnp.shape)
        return

    def loadDS_np(self, f_npfile):
        dsnp = np.load(f_npfile)
        self.feat_scaler.fr = cp.asarray(dsnp[0])
        self.feat_scaler.a  = cp.asarray(dsnp[1])
        self.feat_scaler.b  = cp.asarray(dsnp[2])
        self.engy_scaler.fr = cp.asarray(dsnp[3])
        self.engy_scaler.a  = cp.asarray(dsnp[4])
        self.engy_scaler.b  = cp.asarray(dsnp[5])
        self.feat_a         = cp.asarray(dsnp[6])
        self.engy_a         = cp.asarray(dsnp[7])

        print('DataScaler.loadDS_np from', f_npfile, dsnp.dtype, dsnp.shape)
        #for i in range(dsnp.shape[0]):
        #    print("dsnp[i]",i, dsnp[i].shape)
        return

#================================================================================
from func_feat import get_feat_nbxyz, get_feat_dfeat_nbxyz
from func_io import r_atom_loc_struct
#================================================================================
def get_feat_scl_nbxyz(itypes, nNb, idxNb, nbxyz, data_scaler):
    """ get feature of feature scaled by feat_scaler 
            from local neigbor xyz struture
    """
    feat = get_feat_nbxyz(itypes, nNb, idxNb, nbxyz)
    feat_scaled = data_scaler.pre_feat(feat)
    return feat_scaled

def get_feat_dfeat_scl_nbxyz(itypes, nNb, idxNb, nbxyz, data_scaler):
    """ get feature & derivative of feature scaled by feat_scaler 
            from local neigbor xyz struture
        For nn_MD calculation
    """
    feat, dXidRl, dXindRl = \
            get_feat_dfeat_nbxyz(itypes, nNb, idxNb, nbxyz)

    feat_scaled = data_scaler.pre_feat(feat)
    feat_d1, feat_d2 = data_scaler.pre_dfeat(dXidRl, dXindRl)

    return feat_scaled, feat_d1, feat_d2

def get_feat_dfeat_scl_df(df_orig, data_scaler):
    """ get feature & derivative of feature scaled by feat_scaler 
            from df_orig
        For trainning process
    """
    itypes, engy, fors, nNb, idxNb, nbxyz, err = r_atom_loc_struct(df_orig)

    engy = engy.reshape([engy.size,1])  # important !!!
    engy_scaled = data_scaler.pre_Ei(engy)
    fors_scaled = data_scaler.pre_Fi(fors)
    
    feat_scaled, feat_d1, feat_d2 = \
            get_feat_dfeat_scl_nbxyz(itypes, nNb, idxNb, nbxyz, data_scaler)
    
    return engy_scaled, fors_scaled, feat_scaled, feat_d1, feat_d2, idxNb, engy, fors
#================================================================================
