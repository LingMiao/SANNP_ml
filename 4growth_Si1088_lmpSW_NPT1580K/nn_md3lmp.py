#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 20 12:15:58 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

import parameters as pm
if pm.cupyMD:
    import cupy as cp
else:  # use tf version
    import numpy as cp
    import tensorflow as tf

import numpy as np
import pandas as pd
import sys
import time

from func_data_scaler import mae, mse, mse4, get_feat_dfeat_scl_nbxyz
from func_struct import Atoms_cupy, get_neighbor_struct_cp

from ase import Atoms
from ase.io import read, write
from ase.units import fs, Bohr, eV, Ang, kB
#print('fs, Bohr, eV, Ang, kB\n', fs, Bohr, eV, Ang, kB)
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from ase.calculators.lammpslib import LAMMPSlib

from nn_md import NNMD
from nn_check import NNcheck

#class NNMD3lmp(NNcheck):
class NNMD3lmp(NNMD):
    """
    NNMD3lmp, add lammps to evaluate Ei and Fi to compare EiNN

    parameters:

    """

    #===========================================================================
    def __init__(self, nn, data_scaler, calc_type='lmp_nn', lmpcmds='tersoff', Ei_shift=0, lmp_log='log.NNMD3lmp'):

        super(NNMD3lmp, self).__init__( nn=nn, data_scaler=data_scaler)
        
        self.calc_type = calc_type
        self.Ei_shift  = Ei_shift
        print('NNMD3lmp \n  EiNN will use calcultor mode and ff_style', calc_type, lmpcmds)
        # overset None for setting ase_LAMMPSlib
        self.atoms_ase = read(pm.f_atoms)
        if lmpcmds == 'tersoff':
            self.lmpcmds = ["pair_style tersoff",
                    "pair_coeff * * Si.tersoff Si"]
        elif lmpcmds == 'sw':
            self.lmpcmds = ["pair_style sw",
                    "pair_coeff * * Si.sw Si"]
        elif lmpcmds == 'edip':
            self.lmpcmds = ["pair_style edip",
                    "pair_coeff * * Si.edip Si"]
        elif lmpcmds == 'reax':
            # NOTE "units real", default "units metal"
            #self.lmpcmds = ["units real", 
            self.lmpcmds = ["pair_style reax/c NULL",
                    "pair_coeff * * Si.reax Si",
                    "fix 5 all qeq/reax 1 0.0 10.0 1.0e-6 reax/c"]
        else:
            print('self.lmpcmds', self.lmpcmds)
            self.lmpcmds = lmpcmds

        self.lammps = LAMMPSlib(lmpcmds=self.lmpcmds, log_file=lmp_log, keep_alive=True)
        print('self.lammps, self.atoms_ase', self.lammps, self.atoms_ase)
        self.atoms_ase.set_calculator(self.lammps)
        print("Etot_lmp (eV)", self.atoms_ase.get_potential_energy(),\
                "using", self.atoms_ase.get_calculator() )
        #        "\nForces (eV/A) \n", self.atoms_ase.get_forces())
        return
    #===========================================================================
    def get_potential_energy(self):
        self.lammps.restart_lammps(self.atoms_ase)
        # self.atoms_ase always in CPU
        if pm.cupyMD:
            cell= cp.asnumpy(self.atoms_gpu.cell)
            pos = cp.asnumpy(self.atoms_gpu.positions)
        self.atoms_ase.set_cell(cell, scale_atoms=True)
        self.atoms_ase.set_positions(pos)

        Ep_tot= cp.asarray(self.atoms_ase.get_potential_energy()  ) # eV
        return Ep_tot

    #===========================================================================
    def getEpFp_lmp(self, itype, cell, pos): 
        self.lammps.restart_lammps(self.atoms_ase)
        # self.atoms_ase always in CPU
        if pm.cupyMD:
            cell= cp.asnumpy(cell)
            pos = cp.asnumpy(pos)
        self.atoms_ase.set_cell(cell, scale_atoms=True)
        self.atoms_ase.set_positions(pos)

        Etot_lmp = cp.asarray(self.atoms_ase.get_potential_energy()  ) # eV
        Ei_lmp   = cp.asarray(self.atoms_ase.get_potential_energies()) # eV
        # NOTE -f, to agree with nnff code
        Fi_lmp   = - cp.asarray(self.atoms_ase.get_forces()            ) # eV/A
        #print("Energy (eV)", Etot_lmp, 
        #        np.sum(Ei_lmp), Ei_lmp[:3], Fi_lmp[:3] )
        return Ei_lmp[:,cp.newaxis], Fi_lmp, Etot_lmp

    #===========================================================================
    def getEpFp_uc(self, itype, cell, pos): 
        if self.calc_type=='nn':
            Ep, Fp, Eptot = super(NNMD3lmp, self).getEpFp_uc(itype, cell, pos)
            #return Ep, Fp, Eptot
            Ep2 = Ep -cp.mean(Ep)
            return Ep2, Fp, cp.sum(Ep2)

        if self.calc_type=='lmp':
            Ei_lmp, Fi_lmp, Etot_lmp = self.getEpFp_lmp(itype, cell, pos)
            Ei_lmp2 = Ei_lmp -cp.mean(Ei_lmp) + self.Ei_shift
            print("cp.mean(Ei_lmp), Etot_lmp", cp.mean(Ei_lmp), Etot_lmp)
    #===========================================================================
    def getEpFp_lmp(self, itype, cell, pos): 
        self.lammps.restart_lammps(self.atoms_ase)
        # self.atoms_ase always in CPU
        if pm.cupyMD:
            cell= cp.asnumpy(cell)
            pos = cp.asnumpy(pos)
        self.atoms_ase.set_cell(cell, scale_atoms=True)
        self.atoms_ase.set_positions(pos)

        Etot_lmp = cp.asarray(self.atoms_ase.get_potential_energy()  ) # eV
        Ei_lmp   = cp.asarray(self.atoms_ase.get_potential_energies()) # eV
        # NOTE -f, to agree with nnff code
        Fi_lmp   = - cp.asarray(self.atoms_ase.get_forces()            ) # eV/A
        #print("Energy (eV)", Etot_lmp, 
        #        np.sum(Ei_lmp), Ei_lmp[:3], Fi_lmp[:3] )
        return Ei_lmp[:,cp.newaxis], Fi_lmp, Etot_lmp

    #===========================================================================
    def getEpFp_uc(self, itype, cell, pos): 
        if self.calc_type=='nn':
            Ep, Fp, Eptot = super(NNMD3lmp, self).getEpFp_uc(itype, cell, pos)
            #return Ep, Fp, Eptot
            Ep2 = Ep -cp.mean(Ep)
            return Ep2, Fp, cp.sum(Ep2)

        if self.calc_type=='lmp':
            Ei_lmp, Fi_lmp, Etot_lmp = self.getEpFp_lmp(itype, cell, pos)
            Ei_lmp2 = Ei_lmp -cp.mean(Ei_lmp) + self.Ei_shift
            print("cp.mean(Ei_lmp), Etot_lmp", cp.mean(Ei_lmp), Etot_lmp)
            #return Ei_lmp, Fi_lmp, cp.sum(Ei_lmp)
            return Ei_lmp2, Fi_lmp, cp.sum(Ei_lmp)

        if self.calc_type=='nn_lmp':
            Ep, Fp, Eptot = super(NNMD3lmp, self).getEpFp_uc(itype, cell, pos)

            Ei_lmp, Fi_lmp, Etot_lmp = self.getEpFp_lmp(itype, cell, pos)
            Ei_lmp2 = Ei_lmp -cp.mean(Ei_lmp) +cp.mean(Ep)
            print("Ei_MAE_nn_lmp", Etot_lmp, mae(Ep, Ei_lmp2), mse4(Ep, Ei_lmp2 ))
            return Ep, Fp, Eptot

        if self.calc_type=='lmp_nn':
            Ep, Fp, Eptot = super(NNMD3lmp, self).getEpFp_uc(itype, cell, pos)

            Ei_lmp, Fi_lmp, Etot_lmp = self.getEpFp_lmp(itype, cell, pos)
            Ei_lmp2 = Ei_lmp -cp.mean(Ei_lmp) +cp.mean(Ep)
            print("Ei_MAE_nn_lmp", Eptot, mae(Ep, Ei_lmp2), mse4(Ep, Ei_lmp2 ))
            return Ei_lmp2, Fi_lmp, cp.sum(Ei_lmp2)

    #===========================================================================
