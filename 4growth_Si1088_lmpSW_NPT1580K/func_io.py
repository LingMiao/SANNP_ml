#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Process data io
External APIs: 
    r_atom_loc_struct()
    r_feat_csv()
    gen_feats_rndImg()

Created on Wed Aug 22 10:58:18 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

import numpy as np
import pandas as pd

import os
import time
import sys

import parameters as pm
read_feat_dtype = pm.tf_dtype # 'float32' / 'float64'

#================================================================================
def r_atom_loc_struct(df_orig):
    """ read atomic local structure from Fortran code generated pd data frames
    """
    df_orig[[0,1]] = df_orig[[0,1]].astype(int)
    df_orig.iloc[:,6:(6+pm.maxNb)] = df_orig.iloc[:,6:(6+pm.maxNb)].astype(int)
    itypes= df_orig[0].values         # type of i-th atom
    nNb   = df_orig[1].values         # number of neighbors
    engy  = df_orig[2].values         # energies
    fors  = df_orig[[3,4,5]].values   # forces
    idxNb = df_orig.iloc[:,6:(6+pm.maxNb)].values  # indices of the neighbors
    nbxyz = df_orig.iloc[:,(6+pm.maxNb):].values.reshape([-1,pm.maxNb,3])     # nbxyzinates
    
    tmp1 = np.sum(nbxyz!=0,axis=1).T
    tmp2 = np.sum(idxNb!=0,axis=1)
    tmp3 = nNb
    err = False
    if np.sum(tmp1-tmp2)!=0:
        print("errors between nbxyz and idxNb")
        err = True
    if np.sum(tmp2-tmp3)!=0:
        print("errors between nNb and idxNb")
        err = True
    
    return itypes,engy,fors,nNb,idxNb,nbxyz, err

#================================================================================
def r_feat_csv(f_feat):
    """ read feature and energy from pandas data
    """
    df   = pd.read_csv(f_feat,header=None,index_col=False,dtype=read_feat_dtype)
    itypes = df[1].values.astype(int)
    engy = df[2].values
    feat = df.drop([0,1,2],axis=1).values 
    engy = engy.reshape([engy.size,1])
    return itypes,feat,engy

#================================================================================
def config2xsf(f_config, f_xsf):
    """read config/MOVEMENT file, write to xsf
    """
    # input
    f_cfg = open(f_config,'r')

    line = f_cfg.readline().split()
    natoms = int(line[0])
    
    while True:
        if f_cfg.readline().split()[0]=="Lattice" :  # line of Lattice vector
            break
    cell = np.zeros((3,3))
    cell[0,:] = np.array(f_cfg.readline().split(),dtype=float)
    cell[1,:] = np.array(f_cfg.readline().split(),dtype=float)
    cell[2,:] = np.array(f_cfg.readline().split(),dtype=float)
    
    f_cfg.readline()  # line of Position
    itype = np.zeros(natoms,dtype=int)
    R_cry = np.zeros((natoms,3))
    for i in range(natoms):
        line = f_cfg.readline().split()
        itype[i] = np.array(line[0], dtype=int)
        R_cry[i,:] = np.array(line[1:4], dtype=float)
    #R_cry[R_cry> 1] = R_cry[R_cry> 1] - np.floor(R_cry[R_cry> 1])
    #R_cry[R_cry< 0] = R_cry[R_cry< 0] - np.floor(R_cry[R_cry< 0])

    f_cfg.close()

    # convert
    pos = np.zeros((natoms,3))
    for i in range(natoms):
        pos[i,0] = np.sum(R_cry[i,:] * cell[:,0])
        pos[i,1] = np.sum(R_cry[i,:] * cell[:,1])
        pos[i,2] = np.sum(R_cry[i,:] * cell[:,2])

    # output
    w_xsf(f_xsf, itype, cell, pos)

    return
#================================================================================
def w_xsf(f_xsf, itype, cell, pos): 
    np.set_printoptions(threshold=np.NaN)
    np.set_printoptions(formatter={'float': '{: 16.12f}'.format})

    natoms = itype.shape[0]
    f_x = open(f_xsf,'w')
    f_x.write('CRYSTAL\n')
    f_x.write('PRIMVEC 1\n')
    for i in range(3):
        for j in range(3):
            f_x.write(str(cell[i,j])+'\t')
        f_x.write('\n')

    f_x.write('CONVVEC 1\n')
    for i in range(3):
        for j in range(3):
            f_x.write(str(cell[i,j])+'\t')
        f_x.write('\n')

    f_x.write('PRIMCOORD 1\n')
    f_x.write(str(natoms) +' 1\n')
    for i in range(natoms):
        f_x.write(str(itype[i])+'\t')
        for j in range(3):
            f_x.write(str(pos[i,j])+'\t')
        f_x.write('0.0 0.0 0.0\n')

    f_x.close()
    return
#================================================================================
# test
#config2xsf("final.config", "test.xsf")
