#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Modified on Thu Aug 23 23:37:47 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

import parameters as pm
from func_data_scaler import DataScaler
from nn_model_cupy import EiNN_cupy
from nn_md         import NNMD
from nn_md3lmp     import NNMD3lmp
from func_io2      import *

import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
os.environ["CUDA_VISIBLE_DEVICES"] = pm.cuda_dev
os.system('cat parameters.py')
#=======================================================================
nn = EiNN_cupy(f_Wij_np=pm.f_Wij_np)
data_scaler = DataScaler(f_ds=pm.f_data_scaler, f_feat=pm.f_pretr_feat+'no')

nnmd = NNMD3lmp(nn, data_scaler, calc_type='lmp', lmpcmds='sw')
#nnmd = NNMD3lmp(nn, data_scaler, calc_type='lmp', lmpcmds='tersoff')
#nnmd.NVE_vv(pm.f_atoms, pm.f_initial, b_restart=False, dt=pm.dt, steps=pm.steps, f_traj=None)
#nnmd.NVT_berendsen(pm.f_atoms, pm.f_initial, b_restart=pm.b_restart, f_restart=pm.f_restart, dt=pm.dt, steps=pm.steps, tempEnd=pm.tempEnd)
nnmd.NPT_berendsen(pm.f_atoms, pm.f_initial, b_restart=pm.b_restart, f_restart=pm.f_restart, dt=pm.dt, steps=pm.steps, tempEnd=pm.tempEnd, f_traj=pm.f_traj)

