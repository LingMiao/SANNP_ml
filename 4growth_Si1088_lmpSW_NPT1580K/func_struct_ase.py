#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon 3/26/2018 19:39:55 
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

import numpy as np

from ase import Atoms
from ase.neighborlist import neighbor_list
import collections

import parameters as pm

#================================================================================
def get_neighbor_struct(atoms, R0=pm.R0, Rc=pm.Rc, maxNb=pm.maxNb, self_interaction=False):
    """Analysis atomic structure to get local neighbor structure for each atom.
         If num_nb_atoms > maxNb in Rc, neighbors_list are sorted with dij, 
         and will remain nearest maxNb atoms, and may generate un-exact feature! 
    """
    itype = atoms.get_atomic_numbers()

    nl = neighbor_list('ijDd', atoms, [Rc/2 for atom in atoms], self_interaction)
   
    natoms = len(atoms)
    nNb   = np.zeros((natoms, 1))
    idxNb = np.zeros((natoms, maxNb))
    nbxyz = np.zeros((natoms, maxNb, 3))

    nl_i_jnum = collections.Counter(nl[0])
    i_posmin = 0
    i_posmax = 0
    for iatom in range(natoms):
        ij_max = nl_i_jnum[iatom]

        i_posmax = i_posmin +ij_max
        if (np.min(nl[3][i_posmin:i_posmax]) < R0) :
            print("===Warning: d < R0)", np.min(nl[3][i_posmin:i_posmax]) ,'<', R0, \
                    " of atom", iatom, " in get_neighbor_struct().", \
                    " Will generate wrong feature!" )
        if (nl_i_jnum[iatom] > maxNb):
            # sort as d and make a cutoff of maxNb atoms
            print("===Warning: nl_i_jnum[", iatom, "] ", nl_i_jnum[iatom] ,'> maxNb', maxNb, \
                    " of atom", iatom, " in get_neighbor_struct().", \
                    " Will remain nearest maxNb atoms, and generate un-exact feature!" )
            arg_id    = np.argsort(nl[3][i_posmin:i_posmax])
            nl[1][i_posmin:i_posmax] = nl[1][i_posmin + arg_id]
            nl[2][i_posmin:i_posmax] = nl[2][i_posmin + arg_id]
            nl[3][i_posmin:i_posmax] = nl[3][i_posmin + arg_id]
            ij_max = maxNb
            i_posmax = i_posmin +ij_max

        # sort as idx
        nNb[iatom,0] =  nl_i_jnum[iatom]
        arg_idxNb    = np.argsort(nl[1][i_posmin:i_posmax])  
        # NOET: sort idx and +1 to compare easily with Fortran code
        idxNb[iatom, 0:ij_max]    = nl[1][i_posmin + arg_idxNb] +1 
        nbxyz[iatom, 0:ij_max, :] = nl[2][i_posmin + arg_idxNb]  
        
        i_posmin += nl_i_jnum[iatom] # next atom i

    return itype, nNb, idxNb, nbxyz

#================================================================================
def get_neighbors_nbxyz_debug(atoms, Rc=pm.Rc, maxNb=pm.maxNb, self_interaction=False):
    """ This is OLD version, for debug only.
    """
    nl = neighbor_list('ijD', atoms, [Rc/2 for atom in atoms], self_interaction)
    #print(nl[0][0:46], np.sort(nl[1][0:46])+1, nl[2].shape)
    # print(nl[0].shape, nl[1].shape, nl[2].shape)
   
    natoms = len(atoms)
    idxNb = np.zeros((natoms, maxNb))
    nbxyz = np.zeros((natoms, maxNb, 3))

    nl_i_jnum = collections.Counter(nl[0])
    i_posmin = 0
    i_posmax = 0
    for iatom in range(natoms):
        #print('nl_i_jnum[iatom]: ', nl_i_jnum[iatom])
        ij_max = maxNb if (maxNb < nl_i_jnum[iatom]) else nl_i_jnum[iatom]
        i_posmax = i_posmin +ij_max

        # +1 for consistent with lww's fortran code
        # print(iatom, nl_i_jnum[iatom], np.sort( nl[1][i_posmin:i_posmax]) +1 )
        idxNb[iatom, 0:ij_max]    = nl[1][i_posmin:i_posmax] +1  #could not use sort
        nbxyz[iatom, 0:ij_max, :] = nl[2][i_posmin:i_posmax]
        #print(nl[0][i_posmin:i_posmax+1], nl[1][i_posmin], nbxyz[iatom, 0:3])
        i_posmin += nl_i_jnum[iatom] # next atom i

    return idxNb, nbxyz, nl_i_jnum

#================================================================================
def get_idxNb_inv(nNb, idxNb):
    """ 
    """
    idxNb_inv = np.zeros_like(idxNb)
    np.set_printoptions(threshold=np.NaN)
    # print(idxNb, '\n =========================')
    for q in range(idxNb.shape[0]):
        n = int(nNb[q])
        h2i = idxNb[q,:n].astype(int)
        idxNb_inv[q, :n] = np.where( idxNb[h2i-1]==q+1 )[1]
        # print(q+1, 'th atom,', n, 'nNb, idxNb[q,:n]', h2i)
        # print('i=idxNb(h,q)-1 ', h2i-1, ' th lines as following')
        # print(idxNb[h2i-1])
        # print('where( idxNb[h2i-1]==q+1 )', idxNb_inv[q])
        # print('#==================')
    return idxNb_inv

#================================================================================
def test():
    from ase.io import read, write
    atoms = read("test.xsf")
    itype, nNb, idxNb, nbxyz = get_neighbor_struct(atoms)#, R0=pm.R0, Rc=3.2, maxNb=13)
    get_idxNb_inv(nNb, idxNb)

# test()
