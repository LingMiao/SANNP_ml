#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Modified on Thu Aug 23 23:37:47 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

import parameters as pm
from func_data_scaler import DataScaler
from nn_model_cupy import EiNN_cupy
from nn_md         import NNMD
from nn_check      import NNcheck
#from func_io2      import mdtraj_EiP, mdtraj_EiPFV
from func_io2      import *

import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
os.environ["CUDA_VISIBLE_DEVICES"] = pm.cuda_dev
os.system('cat parameters.py')

#=======================================================================
#nn = EiNN_cupy(f_Wij_np=pm.f_Wij_np)
#nn = EiNN_cupy(f_Wij_np='./NNmodel/Wij.npy_0Fi.npy')
nn = EiNN_cupy(f_Wij_np='./NNmodel/Wij.npy5EiFi.npy')
data_scaler = DataScaler(f_ds=pm.f_data_scaler, f_feat=pm.f_pretr_feat+'no')

nnmd = NNcheck(nn, data_scaler)

#nnmd.check_MDtraj(pm.f_atoms, './Si204_PWmat/MVMTnpt800K_1i_PFVEi', cls_traj=MVMT_EiPFV_cell, f_nn2='MVMTnpt800K_1i_PFVEi_nn5FiEi', maximg=20000, img_skip=1, b_dE_sift=True, f_cellz='./Si204_PWmat/MVMTnpt800K_1i_cellz')
#nnmd.check_MDtraj(pm.f_atoms, './Si204_PWmat/MVMTnpt800K_1i_PFVEi', cls_traj=MVMT_EiPFV_cell, f_nn2='MVMTnpt800K_1i_PFVEi_nn0Fi', maximg=20000, img_skip=1, b_dE_sift=True, f_cellz='./Si204_PWmat/MVMTnpt800K_1i_cellz')
nnmd.check_MDtraj(pm.f_atoms, './Si204_PWmat/MVMTnpt800K_1i_PFVEi', cls_traj=MVMT_EiPFV_cell, f_nn2='MVMTnpt800K_1i_PFVEi_nn5EiFi3', maximg=20000, img_skip=1, b_dE_sift=True, f_cellz='./Si204_PWmat/MVMTnpt800K_1i_cellz')
