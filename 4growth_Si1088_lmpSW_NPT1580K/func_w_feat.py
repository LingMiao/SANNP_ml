#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Process data io
External APIs: 
    r_atom_loc_struct()
    r_feat_csv()
    gen_feats_rndImg()

Created on Wed Aug 22 10:58:18 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

import numpy as np
import pandas as pd

import os
import time
import sys

import parameters as pm
from func_io import r_atom_loc_struct, r_feat_csv
from func_struct import get_neighbor_struct_cp

if pm.ntypes == 1:
    from func_feat import get_feat_nbxyz, get_feat_dfeat_nbxyz
    print('pm.ntypes =1, from func_feat import get_feat_nbxyz, get_feat_dfeat_nbxyz')
else:
    from func_feat_ntype import get_feat_nbxyz, get_feat_dfeat_nbxyz
    print('pm.ntypes >1, from func_feat_ntype import get_feat_nbxyz, get_feat_dfeat_nbxyz')
#================================================================================
def check_Eisum(engy, Ep, errEp=0.1):
    nImage = Ep.shape[0]

    EiTot = np.zeros(nImage)
    iEsum_err = np.zeros(nImage)
    Esum_err = 0
    for i in range(nImage):
        EiTot[i] = np.sum(engy[i]) 
        # if err == True:
        #     print("err EiTot[i]", i, EiTot[i])
        #     EiTot[i] -= 1.0 # will be screened by following eMask

        iEsum_err[i] = Ep[i] - EiTot[i]
        # if (i>20) & (np.abs(iEsum_err[i] -Esum_err) < errEp) :
        #     print(i+1, "th img, np.abs(iEsum_err -Esum_err) < errEp, break!")
        #     break
        #Esum_err = np.mean(iEsum_err[:i])
    
    Esum_err = np.mean(iEsum_err[:i])
    eMask = np.abs(Ep-EiTot-Esum_err)< errEp
    eIdx = np.where(eMask)[0]

    print("Esum_err:", Esum_err)
    print("eMask(d_Esum>err)", np.where(np.abs(Ep-EiTot-Esum_err)> errEp)[0])
    sys.stdout.flush()
    return Esum_err, eIdx

#================================================================================
def check_MVNL_Eisum(f_MV_NL, f_Etot, natoms, errEp=0.1):
    """ check PWmat decomposed Ei ?= Etot
        f_MV_NL='MOVEMENT_NL_lwwf'
        f_Etot='potentialEnergy'
    """
    print("check PWmat decomposed Ei ?= Etot of MOVEMENT_NL_lwwf at ", time.ctime())
    sys.stdout.flush()

    engy = np.loadtxt(f_MV_NL, usecols=(2), unpack=True).reshape([-1,natoms])
    print("checking, engy", engy.shape, '\n', engy[0:2,0:5], time.ctime())
    Ep = np.loadtxt(f_Etot, usecols=(9), unpack=True)

    return check_Eisum(engy, Ep, errEp)
    
#================================================================================
def check_PFVEi_Eisum(f_PFVE, f_Etot, natoms, errEp=0.1):
    """ check PWmat decomposed Ei ?= Etot
        f_PFVE='MOVEMENT_PFVEi'
        f_Etot='potentialEnergy'
    """
    print("check PWmat decomposed Ei ?= Etot of MOVEMENT_NL_lwwf at ", time.ctime())
    sys.stdout.flush()

    engy = (np.loadtxt(f_PFVE, usecols=(1), unpack=True).reshape([-1,4,natoms]))[:,3]
    print("checking, engy", engy.shape, '\n', engy[0:2,0:5], time.ctime())
    Ep = np.loadtxt(f_Etot, usecols=(9), unpack=True)

    return check_Eisum(engy, Ep, errEp)

#================================================================================
def rand_Img1(eIdx, nI_pre, nI_test):
    """ the time-last 100 images in abMD calculation for test
        other images for trainning,
        and choose frist rand 100 images to Pre-train NN 
    """
    idxTest = eIdx[-nI_test:]
    idxTrainVald = eIdx[:-nI_test]          
    np.random.shuffle(idxTrainVald)
    np.random.shuffle(idxTrainVald)
    np.random.shuffle(idxTrainVald)
    idxPretrain= idxTrainVald[:nI_pre]     
    
    print("idxTest.shape, idxTrainVald.shape: ", idxTest.shape, idxTrainVald.shape)
    print("Pre-train set iterations:\n ", str(idxPretrain))
    print("Training and Validation set iterations:\n", str(idxTrainVald))
    print("Test set iterations:\n", str(idxTest))

    return idxPretrain, idxTrainVald, idxTest
    
#================================================================================
def rand_Img_test(eIdx, nI_pre, nI_test):
    """ also choose rand 100 images in abMD calculation for test
        other images for trainning,
        and choose frist rand 100 images to Pre-train NN 
    """
    np.random.shuffle(eIdx)
    np.random.shuffle(eIdx)
    np.random.shuffle(eIdx)
    idxPretrain= eIdx[:nI_pre]     
    idxTrainVald = eIdx[:-nI_test]          
    idxTest = eIdx[-nI_test:]
    
    print("idxTest.shape, idxTrainVald.shape: ", idxTest.shape, idxTrainVald.shape)
    print("Pre-train set iterations:\n ", str(idxPretrain))
    print("Training and Validation set iterations:\n", str(idxTrainVald))
    print("Test set iterations:\n", str(idxTest))

    return idxPretrain, idxTrainVald, idxTest
    
#================================================================================
def rand_Img_read(f_getFeat_out):
    #os.system("/home/lingmiao/bin/get_idx_imgs.sh NN09v5getFeat_randtest.out")
    os.system("/home/lingmiao/bin/get_idx_imgs.sh " +f_getFeat_out)
    
    idxPretrain = pd.read_csv("idxPretrain.txt", header=None,index_col=False,dtype="int", \
            names=None, delim_whitespace=True).values[0]
    idxTrainVald =  pd.read_csv("idxTrainVald.txt", header=None,index_col=False,dtype="int", \
            names=None, delim_whitespace=True).values[0]
    idxTest =  pd.read_csv("idxTest.txt", header=None,index_col=False,dtype="int", \
            names=None, delim_whitespace=True).values[0]
    
    print("idxTest.shape, idxTrainVald.shape: ", idxTest.shape, idxTrainVald.shape)
    print("Pre-train set iterations:\n ", str(idxPretrain))
    print("Training and Validation set iterations:\n", str(idxTrainVald))
    print("Test set iterations:\n", str(idxTest))

    return idxPretrain, idxTrainVald, idxTest
#================================================================================
#@profile
def w_feat_df2csv(df, f_feat):
    """ get feature and write to csv from data frame
    """
    itypes, engy, fors, nNb, idxNb, nbxyz, err = r_atom_loc_struct(df)
    feat = get_feat_nbxyz(itypes, nNb, idxNb, nbxyz)
    # below line for debug!!!
    #feat, _,_ = get_feat_dfeat_nbxyz(itypes, nNb, idxNb, nbxyz)

    out = np.concatenate([np.expand_dims(itypes,1), np.expand_dims(engy,1), feat], axis=1)
    df_out = pd.DataFrame(out)
    
    df.to_csv(f_feat, mode='a',header=False,index=False)
    df_out.to_csv(f_feat+'.csv', mode='a', header=False)

    return

#================================================================================
def w_feats_file(f_MV_NL, natoms, idxPretrain, idxTrainVald, idxTest, f_feat):
    """ write feature files pretrain, train, test
    """
    df_chunk = pd.read_csv(f_MV_NL, \
            header=None,index_col=False,dtype="float", \
            delim_whitespace=True)
    
    df_all = df_chunk.values.reshape([-1, natoms, pm.maxNb*4+6])
    print("generate features R0, Rc, maxNb", pm.R0, pm.Rc, pm.maxNb, " at", time.ctime())
    print("checking, df_all ", df_all.shape, '\n', df_all[0:2,0:5,:], time.ctime())
    sys.stdout.flush()
    
    for i in idxTest:
        df = pd.DataFrame(df_all[i])
        w_feat_df2csv(df, f_feat+'_test')

    for i in idxTrainVald:
        df = pd.DataFrame(df_all[i])
        w_feat_df2csv(df, f_feat+'_train')
    
    #for i in idxPretrain:
    #    df = pd.DataFrame(df_all[i])
    #    w_feat_df2csv(df, f_feat+'_pretrain')
    
    nI_pre = idxPretrain.shape[0]
    os.system('head -' +str(nI_pre*natoms)+ ' ' +f_feat+'_train > ' +f_feat+'_pretrain')
    os.system('head -' +str(nI_pre*natoms)+ ' ' +f_feat+'_train.csv > ' +f_feat+'_pretrain.csv')
    
    print("Job finised at ", time.ctime())
    return

#================================================================================
def get_neighbor_df_PFVEi(df_PFVEi, natoms, cell):
    itype = df_PFVEi[0:natoms,0]
    pos   = np.matmul(df_PFVEi[0:natoms, 1:4], cell.T)
    fors  = df_PFVEi[natoms*1:natoms*2, 1:4]
    engy  = df_PFVEi[natoms*3:natoms*4, 1]

    nNb, idxNb, nbxyz = get_neighbor_struct_cp(cell, pos)
    '''
    from func_struct_ase import get_neighbor_struct
    from ase.io import read
    atoms_str = read(pm.f_atoms)
    at_Pos   = df_PFVEi[0:natoms, 1:4]
    atoms_str.set_scaled_positions(at_Pos)
    itype, nNb, idxNb, nbxyz = get_neighbor_struct(atoms_str)
    '''
    nbxyz = nbxyz.reshape([-1, pm.maxNb*3])
    df_NL = np.concatenate([np.expand_dims(itype,1), \
            np.expand_dims(nNb,1), \
            np.expand_dims(engy,1), \
            fors, idxNb, nbxyz], axis=1)
    return df_NL

def w_feats_file_PFVEi(f_atoms, f_PFVEi, idxPretrain, idxTrainVald, idxTest, f_feat):
    """read MOVEMENT_PFVEi, generate atomic local structure, and feature file
    """
    from ase.io import read
    atoms_str = read(f_atoms)
    natoms = len(atoms_str)
    cell = atoms_str.get_cell(complete=True)
    print('cell', cell)

    df_PFVEi = pd.read_csv(f_PFVEi, header=None,index_col=False,dtype="float", \
                    delim_whitespace=True)
    df_all = df_PFVEi.values.reshape([-1, natoms*4, 4])
    print("generate features R0, Rc, maxNb", pm.R0, pm.Rc, pm.maxNb, " at", time.ctime())
    print("checking, df_all ", df_all.shape, '\n', df_all[0:2,0:5,:], time.ctime())
    sys.stdout.flush()

    for i in idxTest:
        df_NL = get_neighbor_df_PFVEi(df_all[i], natoms, cell)
        df = pd.DataFrame(df_NL)
        w_feat_df2csv(df, f_feat+'_test')

    for i in idxTrainVald:
        df_NL = get_neighbor_df_PFVEi(df_all[i], natoms, cell)
        df = pd.DataFrame(df_NL)
        w_feat_df2csv(df, f_feat+'_train')
    
    nI_pre = idxPretrain.shape[0]
    os.system('head -' +str(nI_pre*natoms)+ ' ' +f_feat+'_train > ' +f_feat+'_pretrain')
    os.system('head -' +str(nI_pre*natoms)+ ' ' +f_feat+'_train.csv > ' +f_feat+'_pretrain.csv')
    
    print("Job finised at ", time.ctime())
    return 

#================================================================================
def w_feats_file_2(f_MV_NL, natoms, idxPretrain, idxTrainVald, idxTest, f_feat):
    """ write feature files pretrain, train, test
    """
    df_chunk = pd.read_csv(f_MV_NL, \
            header=None,index_col=False,dtype="float", \
            iterator=True,chunksize=natoms, delim_whitespace=True)
    
    print("generate features R0, Rc, maxNb", pm.R0, pm.Rc, pm.maxNb, " at", time.ctime())
    i = 0
    for df in df_chunk:
        if i in idxTrainVald:
            print('img', i, 'in idxTrainVald')
            w_feat_df2csv(df, f_feat+'_train')
        elif i in idxTest:
            print('img', i, 'in idxTest')
            w_feat_df2csv(df, f_feat+'_test')
        #else:
        #    print(i, 'not generate feature')

        if i in idxPretrain:
            print('img', i, 'in idxPretrain')
            w_feat_df2csv(df, f_feat+'_pretrain')

        i += 1
    #end for df in df_chunk:

    print("Job finised at ", time.ctime())
    sys.stdout.flush()
    return
#================================================================================
# External APIs: gen_feats_rndImg functions
# Morning 8/22/2018
#================================================================================
from func_struct import get_atoms_type
def gen_feats_rndImg(f_MV_NL, f_Etot, natoms, f_feat, errEp=0.1, nI_pre=pm.nI_pre, nI_test=pm.nI_test):
    """ just need revised rand_Img1() function
    """
    _, at_types, _ = get_atoms_type(pm.f_atoms)
    assert np.all(at_types==pm.at_types), "please check at_types in parameter.py, it should be as above value!"
    _, eIdx = check_MVNL_Eisum(f_MV_NL, f_Etot, natoms, errEp=errEp)
    idxPretrain, idxTrainVald, idxTest = rand_Img1(eIdx, nI_pre, nI_test)
    #idxPretrain, idxTrainVald, idxTest = rand_Img_test(eIdx, nI_pre, nI_test)
    #idxPretrain, idxTrainVald, idxTest = rand_Img_read(\
    #                           f_getFeat_out='NN09v5getFeat_randtest.out')
    w_feats_file(f_MV_NL, natoms, idxPretrain, idxTrainVald, idxTest, f_feat)
    return

def gen_feats_rndImg_PFVEi(f_PFVEi, f_Etot, natoms, f_feat, errEp=0.1, nI_pre=pm.nI_pre, nI_test=pm.nI_test):
    """ just need revised rand_Img1() function
    """
    _, at_types, _ = get_atoms_type(pm.f_atoms)
    assert np.all(at_types==pm.at_types), "please check at_types in parameter.py, it should be as above value!"
    _, eIdx = check_PFVEi_Eisum(f_PFVEi, f_Etot, natoms, errEp=errEp)
    idxPretrain, idxTrainVald, idxTest = rand_Img1(eIdx, nI_pre, nI_test)
    #idxPretrain, idxTrainVald, idxTest = rand_Img_test(eIdx, nI_pre, nI_test)
    #idxPretrain, idxTrainVald, idxTest = rand_Img_read(\
    #                           f_getFeat_out='NN09v5getFeat_randtest.out')
    w_feats_file_PFVEi(pm.f_atoms, f_PFVEi, idxPretrain, idxTrainVald, idxTest, f_feat)

    return

def gen_feats_test(f_MV_NL, f_Etot, natoms, f_feat, errEp=0.1, nI_pre=100, nI_test=100):
    """ just need revised rand_Img1() function
    """
    _, at_types, _ = get_atoms_type(pm.f_atoms)
    assert np.all(at_types==pm.at_types), "please check at_types in parameter.py, it should be as above value!"
    eIdx = np.array(range(2))
    idxPretrain, idxTrainVald, idxTest = rand_Img1(eIdx, nI_pre, nI_test)
    w_feats_file_2(f_MV_NL, natoms, idxPretrain, idxTrainVald, idxTest, f_feat)
    return

#================================================================================
def MOVEMENT2f_PFVEi(f_MOVEMENT, f_PFVEi):
    """read MOVEMENT file, write to MOVEMENT_PFVEi
    NOTE: should be very caryfully used and double check f_PFVEi file!!!
       linux cmd like as:
       grep "^ *[1-9]" MOVEMENT | sed "/Iteration/d" | sed "s/    1  1  1//" | sed "s/    0  0  0//" | less
    """
    #cmd = 'grep "^ *[1-9]" ' +f_MOVEMENT+ ' | sed "/Iteration/d" | sed "s/    1  1  1//" | sed "s/    0  0  0//" > ' +f_PFVEi
    cmd = 'grep "^  14" ' +f_MOVEMENT+ ' | sed "/Iteration/d" | sed "s/     1  1  1//" | sed "s/     0  0  0//" > ' +f_PFVEi
    os.system(cmd)
    cmd = 'grep Iter ' +f_MOVEMENT+ ' > potentialEnergy'
    os.system(cmd)
    return
#================================================================================
# test
#gen_feats_rndImg(f_MV_NL=pm.f_MV_NL, f_Etot=pm.f_Etot, natoms=pm.natoms, f_feat=pm.f_feat, nI_pre=pm.nI_pre, nI_test=pm.nI_test)
#MOVEMENT2f_PFVEi(pm.f_MVMT, pm.f_PFVEi)
#gen_feats_rndImg_PFVEi(f_PFVEi=pm.f_PFVEi, f_Etot=pm.f_Etot, natoms=pm.natoms, f_feat=pm.f_feat, nI_pre=pm.nI_pre, nI_test=pm.nI_test)
#gen_feats_test(f_MV_NL=pm.f_MV_NL, f_Etot=pm.f_Etot, natoms=pm.natoms, f_feat='feat_test', nI_pre=1, nI_test=1)


#check_PFVEi_Eisum('./Si204_PWmat/MVMTnpt850K_1i_PFVEi', './Si204_PWmat/MVMTnpt850K_1i_Etot', 204, errEp=0.1)
