#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
#================================================================================
# device related
gpu_mem  = 0.9 # tensorflow used gpu memory
cuda_dev = '0' # unoccupied gpu, using 'nvidia-smi' cmd
cupyFeat = True# if use GPU cupy lib to calculate feature, set True
cupyMD   = True# if use md-cupy code, set True
tf_dtype = 'float32' # dtype of tensorflow trainning, 'float32' faster than 'float64'
#================================================================================
# dft_MOVEMENT related
#f_atoms = 'Si1088_111step765.cif'
f_atoms = 'Si1088_111step765_c40.7.cif'
f_fix   = 'Si1088_111step765.fix'
natoms  = 1088  # num of atoms in unit cell 
ntypes  = 1    # num of types of atoms in unit cell 
at_types = np.array([[14, 1088]])
#at_types = np.array([[14, 204]])

nImage  = 1000 # num of images in MD configurations, ie. MOVEMENT file of PWmat
nI_pre  = 100  # num of images for NN pretrain
nI_test = 100  # num of images for NN test

#================================================================================
# feature related
maxNb   = 80   # max num of neighbor atoms
Rc      = 6.2  # Ang, max distance of neighbor atoms
R0      = 1.8  # Ang, min distance of neighbor atoms
nLPP2b = np.array([36])    # num of feature 1 (2 body pair)
nLPP3b = np.array([6])     # num of feature 2 (3 body pair)
ntpij  = int(ntypes*(ntypes+1)/2)
nFeats = nLPP2b*ntypes + nLPP3b*nLPP3b*nLPP3b*ntpij


Rc_Eiloc = 4.2 # Angstrom
maxNb_Eiloc = 24
#================================================================================
# feature file related
f_MV_NL = 'MOVEMENT_NL_lwwf'  # neighbor list generated from Fortran code
f_MVMT  = 'MOVEMENT.test'     # PWmat generated MOVEMENT file
f_PFVEi = 'MOVEMENT_PFVEi'    # 4n *4 file includes n*pos, n*fors, n*vel, n*Ei
f_Etot  = 'potentialEnergy'   # cmd: grep Etot MOVEMENT > potentialEnergy
f_feat  = 'feat1.1_itp'          # prefix of feature files
f_post  = '.csv'              # postfix of feature files
f_pretr_nblt = f_feat +"_pretrain"
f_train_nblt = f_feat +"_train"
f_test_nblt  = f_feat +"_test"
f_pretr_feat = f_feat +"_pretrain"+f_post
f_train_feat = f_feat +"_train"+f_post
f_test_feat  = f_feat +"_test"+f_post
#================================================================================
# NN model related
nNodeL1 = np.array([80])  # nodes of layer 1 of NN
nNodeL2 = np.array([40])  # nodes of layer 2 of NN
# trainning 
learning_rate=0.0001 
rtLossE      = 1.8
rtLossF      = 0.2
epochs_pretrain = 50001
epochs_alltrain = 5001
epochs_Fi_train = 51
iFi_repeat      = 1
eMAE_err = 0.05 # eV

d_nnEi  = './NNEi/'
d_nnFi  = './NNmodel/'
f_Einn_model   = d_nnEi+'allEi_final.ckpt'
f_Finn_model   = d_nnFi+'Fi_final.ckpt'
#================================================================================
# NNMD related 
f_data_scaler = d_nnFi+'data_scaler.npy'
f_Wij_np  = d_nnFi+'Wij.npy5EiFi.npy'
b_restart = 1 # 0: MVMT, 1: restart, 2: xsf/cif
b_newTemp0= True
f_initial = 'MOVEMENT_PFVEi_1st'  # from PWmat calculations MOVEMENT_PFV file
#f_restart = 'traj_nnMD_3x3fix_init_2500K.csv_restart0'
f_restart = 'traj_nnMD.csv_NPT700K_restart0'
savefile = 'md/'
# MD parameters
from ase.units import fs, Bohr, eV, Ang, kB
dt = 1*fs # 
steps = 100001

# NVT
temp0 = 1580   # K, NOTE MaxwellBoltzmannDistribution() in nn_md.py
tempEnd = 1580 # K, for NVT
taut    = 0.5*1000*fs

# NPT
taup    = 120*fs    # smaller means larger scl
stressEnd = 0  # eV/ Ang**3
#n_scl_cell = 10 # 
n_scl_cell = 500 # 

n_print = 1
f_traj  = 'traj_nnMD.csv_sw'+str(temp0)+'K'
#================================================================================
