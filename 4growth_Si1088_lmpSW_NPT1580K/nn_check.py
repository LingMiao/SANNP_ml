#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 20 12:15:58 PDT 2018
@author: lingmiao@lbl.gov at Prof. LinWang Wang's group
"""

import parameters as pm
if pm.cupyMD:
    import cupy as cp
    from nn_model_cupy import NNapiBase #,EiNN_cupy
else:  # use tf version
    import numpy as cp
    import tensorflow as tf
    from nn_model import NNapiBase #,EiNN

import numpy as np
import pandas as pd
import sys
import time

from func_data_scaler import mae, mse, mse4, get_feat_scl_nbxyz, get_feat_dfeat_scl_nbxyz
from func_struct import Atoms_cupy, get_neighbor_struct_cp, get_Epot_local_gs

from ase import Atoms
from ase.io import read, write
from ase.units import fs, Bohr, eV, Ang, kB
#print('fs, Bohr, eV, Ang, kB\n', fs, Bohr, eV, Ang, kB)

class NNcheck(NNapiBase):
    """
    NNMD

    parameters:

    """

    #===========================================================================
    def __init__(self, nn, data_scaler):

        super(NNcheck, self).__init__( nn=nn, data_scaler=data_scaler)
        
        # below vars setted in self.init_structure()
        self.atoms_ase = None
        # and transfer to GPU cupy arrays
        self.atoms_gpu = Atoms_cupy()

    #===========================================================================
    def getEp_uc(self, itype, cell, pos): 
        nNb,idxNb,nbxyz = get_neighbor_struct_cp(cell, pos)
        feat_scaled = get_feat_scl_nbxyz(itype, nNb, idxNb, nbxyz, self.ds)
        
        if pm.cupyMD:
            engy_out = self.nn.getEi(feat_scaled, itype)
        else:  # use tf version
            engy_out = \
                    self.sess.run( \
                    (self.pred_Ei), \
                    feed_dict={self.X:feat_scaled, self.itp:itype})

        Ep = self.ds.post_Ei(engy_out)

        return Ep, cp.sum(Ep)

    #===========================================================================
    def getEpFp_uc(self, itype, cell, pos): 
        nNb,idxNb,nbxyz = get_neighbor_struct_cp(cell, pos)
        feat_scaled, feat_d1, feat_d2 = get_feat_dfeat_scl_nbxyz(itype, nNb, idxNb, nbxyz, self.ds)
        
        if pm.cupyMD:
            engy_out = self.nn.getEi(feat_scaled, itype)
            f_out    = self.nn.getFi(feat_scaled, feat_d1, feat_d2, idxNb)
        else:  # use tf version
            engy_out, f_out = \
                    self.sess.run( \
                    (self.pred_Ei, self.pred_Fi), \
                    feed_dict={self.X:feat_scaled, \
                    self.tfdXi:feat_d1, self.tfdXin:feat_d2, self.tf_idxNb:idxNb, self.itp:self.itp_uc})

        Ep = self.ds.post_Ei(engy_out)
        Fp = self.ds.post_Fi(f_out)

        return Ep, Fp, cp.sum(Ep)

    #===========================================================================
    def check_MDtraj(self, f_atoms, f_traj, cls_traj, f_nn2=None, maximg=10000, img_skip=20, b_dE_sift=True, f_cellz=None):
        '''
        compare with another MDtraj (xyz -> Ei)  
        '''
        
        #=== ini structure ======================
        atoms_nn  = read(f_atoms)
        n_atoms   = len(atoms_nn)
        cell_cpu  = atoms_nn.get_cell()
        itype_cpu = atoms_nn.get_atomic_numbers()

        if f_cellz != None:
            data_traj = cls_traj(cell_cpu, itype_cpu, f_cellz)
        else:
            data_traj = cls_traj(cell_cpu, itype_cpu)
        data_traj.open4read(f_traj, natoms=n_atoms, img_skip=img_skip)
        
        cell  = cp.asarray(cell_cpu)
        itype = cp.asarray(itype_cpu)

        #=== compute and cmp ======================
        print('step, Etot_now, Etot_old, dEtot, maeEi, mae4, Ediff min, max')
        f_xyz = open(f_traj+'_nn5FiEi.xyz', 'wb')

        for i in range(int(maximg/img_skip)):
            if f_cellz != None:
                Ei_nn1, df_Pos, Fi_nn1, Vi_nn1, cell_cpu = data_traj.r_EiPFV_itr()
                cell  = cp.asarray(cell_cpu)
            else:
                Ei_nn1, df_Pos, Fi_nn1, Vi_nn1 = data_traj.r_EiPFV_itr()
    
            Ei_nn1   = cp.asarray(Ei_nn1)
            Fi_nn1   = cp.asarray(Fi_nn1)
            #Vi_nn1   = cp.asarray(Vi_nn1)
            Etot_nn1 = Ei_nn1.sum()

            atoms_nn.set_cell(cell_cpu, scale_atoms=True)
            atoms_nn.set_positions(df_Pos)
            df_Pos   = cp.asarray( atoms_nn.get_positions(wrap=True) )

            Ei, f, Etot = self.getEpFp_uc(itype, cell, df_Pos)
            Epot_locNN  = get_Epot_local_gs(cell, df_Pos, Ei    )
            Epot_locDFT = get_Epot_local_gs(cell, df_Pos, Ei_nn1)

            if b_dE_sift:
                Ediff = cp.abs(Ei_nn1 -cp.mean(Ei_nn1) -Ei +cp.mean(Ei)) 
            else:
                Ediff = cp.abs(Ei_nn1-Ei) 
            print(str(i*img_skip+1), \
                    Etot, Etot_nn1, Etot -Etot_nn1, \
                    mae(Ei, Ei_nn1), mae(f, Fi_nn1),  mse4(Ei, Ei_nn1, 10), \
                    cp.min(Ediff), cp.max(Ediff), \
                    'at', time.ctime())
            self.mark_Ei_xyz(Ediff, df_Pos, i*img_skip+1, f_xyz, mk_scl=1000, b_print=True, n_dEi=10)

            if f_nn2 != None:
                if pm.cupyMD:
                    Ei      = cp.asnumpy(Ei)
                    pos_out = cp.asnumpy(df_Pos)
                    f       = cp.asnumpy(f)
                    Ei_nn1  = cp.asnumpy(Ei_nn1)
                    Fi_nn1  = cp.asnumpy(Fi_nn1)
                    Epot_locNN  = cp.asnumpy(Epot_locNN)
                    Epot_locDFT = cp.asnumpy(Epot_locDFT)

                out = np.concatenate([Ei, pos_out, -f, Ei -Ei_nn1, -f+Fi_nn1], axis=1)
                df_out = pd.DataFrame(out)
                df_out.to_csv(f_nn2, mode='a', header=False, sep='\t')

                out = np.concatenate([Ei_nn1, Epot_locDFT, Ei, Epot_locNN], axis=1)
                df_out = pd.DataFrame(out)
                df_out.to_csv(f_nn2+'_Eiloc', mode='a', header=False, sep='\t')
                
        f_xyz.close()
        return
        
    #===========================================================================
    def mark_Ei_xyz(self, Ediff, df_Pos, img, f_xyz, mk_scl=1000, b_print=True, n_dEi=10):
        '''
        Find largest mk_natoms diff_Ei1v2, change its type to mk_type,
        and output to f_xyz
        #Ediff = cp.abs(Ei1-Ei2) 
        '''
        atoms_type = cp.asarray(cp.copy(self.itp_uc)[:,np.newaxis])

        atoms_type += (Ediff *mk_scl).astype(int)
        ixyz = cp.hstack((atoms_type, df_Pos[:]))

        if b_print:
            Ediff_sortidx = cp.argsort(Ediff, axis=0)
            idx_largeEi   = Ediff_sortidx[-n_dEi:, 0]
            #print(n_dEi, 'large dEi', "{:2.4f}".format(Ediff[idx_largeEi]))
            print(n_dEi, 'large dEi', Ediff[idx_largeEi].flatten())

        if pm.cupyMD:
            ixyz = cp.asnumpy(ixyz)
        np.savetxt(f_xyz, ixyz, fmt='%d %4.3f %4.3f %4.3f', delimiter=' ', \
                header=str(self.natoms)+'\n markEi step '+ str(img), comments='')
        return

    #===========================================================================
    def mark_Ei_xyz_v2(self, Ediff, df_Pos, img, f_xyz, mk_natoms=20, mk_type=32):
        '''
        Find largest mk_natoms diff_Ei1v2, change its type to mk_type,
        and output to f_xyz
        #Ediff = cp.abs(Ei1-Ei2)
        '''
        atoms_type = cp.asarray(cp.copy(self.itp_uc)[:,np.newaxis])

        Ediff_sortidx = cp.argsort(Ediff, axis=0)
        idx_largeEi   = Ediff_sortidx[-mk_natoms:, 0]
        #print('Ediff_sortidx', Ediff_sortidx.shape, idx_largeEi, \
        #        '\n Ediff', cp.get_array_module(Ediff), Ediff[idx_largeEi])

        atoms_type[ idx_largeEi ] = mk_type
        ixyz = cp.hstack((atoms_type, df_Pos[:]))

        if pm.cupyMD:
            ixyz = cp.asnumpy(ixyz)
        np.savetxt(f_xyz, ixyz, fmt='%d %4.3f %4.3f %4.3f', delimiter=' ', \
                header=str(self.natoms)+'\n markEi step '+ str(img), comments='')
        return

    #===========================================================================
    def check_MDtraj_Epot_local(self, f_atoms, f_traj, cls_traj, f_nn2=None, maximg=10000, img_skip=20, b_dE_sift=True, f_cellz=None):
        '''
        compare with another MDtraj (xyz -> Ei)  
        '''
        
        #=== ini structure ======================
        atoms_nn  = read(f_atoms)
        n_atoms   = len(atoms_nn)
        cell_cpu  = atoms_nn.get_cell()
        itype_cpu = atoms_nn.get_atomic_numbers()

        if f_cellz != None:
            data_traj = cls_traj(cell_cpu, itype_cpu, f_cellz)
        else:
            data_traj = cls_traj(cell_cpu, itype_cpu)
        data_traj.open4read(f_traj, natoms=n_atoms, img_skip=img_skip)
        
        cell  = cp.asarray(cell_cpu)
        itype = cp.asarray(itype_cpu)

        #=== compute and cmp ======================
        print('step, Etot_now, Etot_old, dEtot, maeEi, mae4, Ediff min, max')
        f_xyz = open(f_traj+'_nn5FiEi.xyz', 'wb')

        for i in range(int(maximg/img_skip)):
            if f_cellz != None:
                Ei_nn1, df_Pos, Fi_nn1, Vi_nn1, cell_cpu = data_traj.r_EiPFV_itr()
                cell  = cp.asarray(cell_cpu)
            else:
                Ei_nn1, df_Pos, Fi_nn1, Vi_nn1 = data_traj.r_EiPFV_itr()
    
            Ei_nn1   = cp.asarray(Ei_nn1)
            Fi_nn1   = cp.asarray(Fi_nn1)
            #Vi_nn1   = cp.asarray(Vi_nn1)
            Etot_nn1 = Ei_nn1.sum()

            atoms_nn.set_cell(cell_cpu, scale_atoms=True)
            atoms_nn.set_positions(df_Pos)
            df_Pos   = cp.asarray( atoms_nn.get_positions(wrap=True) )

            #Ei, f, Etot = self.getEpFp_uc(itype, cell, df_Pos)
            Epot_local = get_Epot_local_gs(cell, df_Pos, Ei_nn1)

            print(str(i*img_skip+1), Ei_nn1[:3,0], Epot_local[:3,0] )
            if f_nn2 != None:
                if pm.cupyMD:
                    Ei_nn1     = cp.asnumpy(Ei_nn1)
                    Epot_local = cp.asnumpy(Epot_local)

                out = np.concatenate([Ei_nn1, Epot_local], axis=1)
                df_out = pd.DataFrame(out)
                df_out.to_csv(f_nn2, mode='a', header=False, sep='\t')
                
        return
