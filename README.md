# SANNP_ml - Single Atom Neural Network Potential, ml version
by Ling Miao1, and Lin-Wang Wang2

1 School of Optical and Electronic Information, Huazhong University of Science and Technology, China

2 Lawrence Berkeley National Laboratory

## Descriptions
This code is used to study a far from equilibrium system of liquid to crystal Si growth using Machine learning force field. Full description of the code can be found in:

> Ling Miao, Lin-Wang Wang*, Liquid to crystal Si growth simulation using machine learning force field. Submitted to The Journal of Chemical Physics, 2020.

This code was developed from SANNP (Single Atomic Neural Network Potential), which is a machine learning force field that trains on atomic energies and/or forces. Full description of the original SANNP can be found in:
> https://gitlab.com/yufeng.huang/sannp, and 

> Huang, Y., Kang, J., Goddard III, W.A., Wang, L.-W. (2019) Density functional theory based neural network force fields from energy decompositions. Phys. Rev. B 99, 064103. doi: 10.1103/PhysRevB.99.064103

In the implemntation, the atomic energies are obtained using the partition scheme developed by Wang3,4 and implemented in PWmat (www.pwmat.com). The neural network potential uses piecewise cosine functions as basis functions to build up 2-body and 3-body descriptors as inputs to the neural network. Once the neural network is fitted, atomic energies can be obtained directly, and forces can be obtained by taking derivative of the total energy with respect to the atomic postitions.

3 Wang, L. W. (2002). Charge-Density Patching Method for Unconventional Semiconductor Binary Systems. Physical Review Letters, 88(25), 4.

4 Kang, J., Wang, L. W. (2017). First-principles Green-Kubo method for thermal conductivity calculations. Physical Review B, 96(2), 1–5.

## Instructions

### Requirements
The SANNP code is written in Python3 and uses Numpy, cupy and Tensorflow library. We recommend insalling the necessary packages and libraries via conda.

* Anaconda: https://www.anaconda.com/download/ or
* Miniconda: https://conda.io/docs/user-guide/install

This code was developed with following required libraries:
```
python=3.6.3
matplotlib=2.1.2
numpy=1.14.3
pandas=0.22.0
scikit-learn=0.19.1
scipy=1.0.0
cupy=4.1.0
tensorflow=1.4.1
tensorflow-gpu=1.4.1
```

### Installations
The obtain download SANNP from Gitlab, just type the following in the command line:
```
% git clone https://gitlab.com/LingMiao/sannp_ml.git
```

### Usage
Example data has been included in the Data folder. The script to train using the Data is in the run folder. To run the calculation, you can CD into the run folder:
```
% cd sannp_ml/run
```
and run the calculation using the following command:
```
% bash run.sh > run.out & 
```
