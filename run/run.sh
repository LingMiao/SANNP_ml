#!/bin/bash
#==========================================
# Test the code
# Created on Thu Jun 25 02:09:31 PDT 2020
# Email: miaoling@hust.edu.cn
#==========================================
cd ../data_DFT_NPT_MD
rm MOVEMENTNPT700K_zbk_mini_*

cd ../1generate_features
./func_w_feat.py | tee func_w_feat.out 
ls ../data_DFT_NPT_MD

# to train Wij in ./NNmodel
cd ../2train_NNFF
rm ./NNmodel/* 
./main_train_tf.py | tee main_train_tf.out

# use trained Wij in ./NNmodel
cd ../3growth_Si1088_NNb5Fi_NPT800K
rm traj_nnMD.csv_NPT800K* 
./main_md_cupy.py | tee main_md_cupy.out

# use ase/lammps interface
cd ../4growth_Si1088_lmpSW_NPT1580K
rm traj_nnMD.csv_sw1580K*
./main_md_cupy_lmp2.py | tee main_md_cupy_lmp2.out
